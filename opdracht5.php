<!doctype html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="Javascript/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossorigin="anonymous">

    <title>Opdracht 5 - Week day</title>
</head>

<body>
 <?php
 include_once ("week.php");
 $yearsHTML = GetYearHTML();
 $weeksHTML = GetWeekSelectHTML(date("Y"));
 $template = file_get_contents("Templates/opdracht5_selectform.html");
 $output = str_replace(
     [
        '{years}',
        '{weeks}'
     ],
     [
         $yearsHTML,
         $weeksHTML
     ],
     $template
 );
 echo $output;
 ?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
<script src="Javascript/opdracht5.js"></script>
</body>
</html>