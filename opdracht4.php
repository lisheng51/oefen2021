<!doctype html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="Javascript/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossorigin="anonymous">

    <title>Opdracht 4 - Checkerboard</title>
</head>

<body>
<?php
function Checkerboard() {
    echo '<table class="m-3">';
    echo '<tbody>';
    for ($y = 1; $y <= 10; $y++) {
        echo "<tr>";
        for ($x = 1; $x <= 10; $x++) {
            if (($x + $y) % 2 == 1) {
                $color = "white";
            } else {
                if ($y <= 4) {
                    $color = "blue";
                }
                elseif ($y <= 6) {
                    if ($x == 5) {
                        $color = "yellow";
                    } else {
                        $color = "red";
                    }
                }
                else {
                    $color = "green";
                }
            }
            echo '<td style="width: 50px; height: 50px; border: black solid 2px; background-color:'.$color.';"></td>';
        }
        echo "</tr>";
    }
    echo '</tbody>';
    echo '</table>';
}

Checkerboard();
?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
</body>
</html>