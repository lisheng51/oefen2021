<?php

// Get the json object and make it into a PHP array
$jsonobj = file_get_contents('https://bloemendaalconsultancy.nl/funny_lab/sort/Home/json');
$obj = json_decode($jsonobj);

// Sort the object by province and city
usort($obj->listdb, function ($a, $b) {
    if ($a->province === $b->province) {
        return $a->city <=> $b->city;
    }
    return $a->province <=> $b->province;
});

// Remove duplicate values
$lastProvince = '';
$lastCity = '';
foreach ($obj->listdb as $city) {
    if ($city->city !== $lastCity) {
        $cityList[] = $city;
    } else {
        if ($city->province !== $lastProvince) {
            $cityList[] = $city;
        }
    }
    $lastProvince = $city->province;
    $lastCity = $city->city;
}

// Echo HTML
echo "<ul>";
$lastProvince = '';
foreach ($cityList as $city) {
    if ($city->province !== $lastProvince) {
        echo "<li>{$city->province}: {$city->city}";
    } else {
        echo ",{$city->city}";
    }
    $lastProvince = $city->province;
}
echo "</ul>";