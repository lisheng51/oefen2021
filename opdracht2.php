<!doctype html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="Javascript/jquery-3.6.0.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossorigin="anonymous">

    <title>Opdracht 2</title>
</head>

<body>
<div class="card m-3">
    <h5 class="card-header">Fibonacci</h5>
    <div class="card-body" id="container-form">
        <form id="form" class="form-inline">
            <div class="row col-md-8">
                <div class="form-group col-md-2 mb-2">
                    <input type="number" id="length" class="form-control" name="length" placeholder="Length" min="1" required>
                </div>
                <div class="form-group col-md-2 mb-2">
                    <input type="number" id="modulo" class="form-control" name="modulo" placeholder="Modulo" min="1" required>
                </div>
                <div class="form-group col-md-1">
                    <button type="submit" class="form-control btn btn-primary">Verzenden</button>
                </div>
            </div>
        </form>
    </div>
    <div id="container-result"></div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
<script src="Javascript/opdracht2.js"></script>
</body>
</html>