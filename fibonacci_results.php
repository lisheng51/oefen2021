<?php

// Returns the fibonacci numbers in an array with length equal to length parameter
function Fibonacci(int $length) {
    $array[] = 0;
    if ($length > 1) {
        $array[] = 1;
    }
    for ($counter = 2; $counter < $length; $counter++) {
        $array[] = $array[$counter-2] + $array[$counter-1];
    }
    return $array;
}



$length = "";
$modulo = "";
if (isset($_POST["length"]) && isset($_POST["modulo"])) {
    // Validate input
    try {
        if (!(is_numeric($_POST["length"]) && (int)$_POST["length"] > 0)) {
            $array["statusCode"] = 100;
            $array["message"] = "Please submit a valid length";
            exit(json_encode($array));

        }

        if (!(is_numeric($_POST["modulo"]) && (int)$_POST["modulo"] > 0)) {
            throw new Exception("Please submit a valid modulo");
        }

        // Set and calculate values for step 1, 2 and 3
        $length = $_POST["length"];
        $modulo = $_POST["modulo"];
        $fibonacciArray = Fibonacci($length);
        $fibonacci = '';
        $fibonacciByModulo = '';
        $fibonacciByModuloSum = 0;

        foreach ($fibonacciArray as $value) {
            $fibonacci .= "<span class='badge rounded-pill bg-primary mx-1'>" . $value . "</span>";

            if ($value % $modulo == 0) {
                $fibonacciByModulo .= "<span class='badge rounded-pill bg-primary mx-1'>" . $value . "</span>";
                $fibonacciByModuloSum += $value;
            }
        }
        $fibonacciByModuloSum = "<span class='badge rounded-pill bg-primary mx-1'>" . $fibonacciByModuloSum . "</span>";

        $template = file_get_contents("./Templates/fibonacci_result.html");
        $result = str_replace(
            [
                '{fibonacci}',
                '{fibonacciByModulo}',
                '{fibonacciByModuloSum}'
            ],
            [
                $fibonacci,
                $fibonacciByModulo,
                $fibonacciByModuloSum
            ],
            $template
        );
    }
    catch (Exception $exception) {
        $result = "<h2>$exception->getMessage()</h2>";
    }

    $array["statusCode"] = 100;
    $array["message"] = $result;
    exit(json_encode($array));
}