<?php

class Home extends Su_Controller
{

    public function sql()
    {
        $module = $this->router->module;
        $add_insert_list = ['bc_article_cate', 'bc_article_status'];
        exit($this->global_model->makeSql($module, $add_insert_list));
    }

    public function copiedata()
    {
        $module = $this->router->module;
        exit($this->module_model->copieLiveDataUrl($module));
    }
}
