<?php

class Article extends Back_Controller
{

    protected static $title_module = 'Artikel';

    public function add()
    {
        if ($this->input->post()) {
            $this->addAction();
        }
        $data["rsdb"] = null;
        $data["created_at"] = date('d-m-Y H:i:s');
        $data["title"] = self::$title_module . " toevoegen";
        $data["delButton"] = null;
        $data["event_result_box"] = "";
        $this->view_layout("edit", $data);
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        $rsdb = $this->article_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            $json["msg"] = self::$title_module . " is niet gevonden!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $data["is_del"] = 1;
        $this->article_model->edit($id, $data);
        $json["msg"] = self::$title_module . " is verwijderd!";
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $this->editAction();
        }
        $rsdb = $this->article_model->get_one_by_id($id);
        if (empty($rsdb) === true) {
            redirect($this->controller_url);
        }
        $data["rsdb"] = $rsdb;
        $data["created_at"] = F_datetime::convert_datetime($rsdb["created_at"]);
        $data["title"] = self::$title_module . " wijzigen";
        $data["delButton"] = delButton($this->controller_name . '.del', $id);
        $data["event_result_box"] = "";
        $this->view_layout("edit", $data);
    }

    public function index()
    {
        $data_where[] = [$this->article_model->table . ".is_del" => 0];
        $data_where[] = setFieldAndOperator('search_title', $this->article_model->table . '.title');
        $data_where[] = setFieldAndOperator('search_description', $this->article_model->table . '.description');
        $data_where[] = setFieldAndOperator($this->article_cate_model->primary_key, $this->article_model->table . '.' . $this->article_cate_model->primary_key);

        $this->article_model->sql_select[] = [$this->article_cate_model->table => 'name as cate_name'];
        $this->article_model->sql_join[] = [$this->article_cate_model->table => $this->article_cate_model->primary_key];

        $this->article_model->sql_select[] = [$this->article_status_model->table => 'label as status_name'];
        $this->article_model->sql_join[] = [$this->article_status_model->table => $this->article_status_model->primary_key];

        $this->article_model->setSqlWhere($data_where);
        $this->article_model->sql_order_by = setFieldOrderBy();
        $total = $this->article_model->get_total();
        $data["listdb"] = $this->getData();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);

        $data["result"] = $this->view_layout_return("ajax_list", $data);
        if ($this->input->post()) {
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }

        $data["title"] = self::$title_module . " overzicht";
        $data["addButton"] = addButton($this->controller_name . '.add', $this->controller_url . '/add');
        $data["event_result_box"] = "";
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->article_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs["created_at"] = F_datetime::convert_datetime($rs["created_at"]);
            $rs["edit_url"] = site_url($this->controller_url . "/edit/{$rs[$this->article_model->primary_key]}");
            $rs["view_url"] = site_url($this->controller_url . "/view/{$rs[$this->article_model->primary_key]}");
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    private function addAction()
    {
        $data = $this->getPostdata();
        $data_where[$this->article_model->table . '.' . $this->article_cate_model->primary_key] = $data[$this->article_cate_model->primary_key];
        $data_where[$this->article_model->table . '.' . 'title'] = $data['title'];
        $this->article_model->sql_where = $data_where;
        $check_double = $this->article_model->get_one();
        if (empty($check_double) === false && $check_double["is_del"] > 0) {
            $data_reset["is_del"] = 0;
            $this->article_model->edit($check_double[$this->article_model->primary_key], $data_reset);
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . " is aangemaakt!";
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }

        if (empty($check_double) === false) {
            $json["msg"] = self::$title_module . " bestaat al!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $insert_id = $this->article_model->add($data);
        if ($insert_id > 0) {
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . " is aangemaakt!";
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function editAction()
    {
        $data = $this->getPostdata();
        $id = $this->input->post($this->article_model->primary_key);

        $data_where[$this->article_model->table . '.' . $this->article_cate_model->primary_key] = $data[$this->article_cate_model->primary_key];
        $data_where[$this->article_model->table . '.' . 'title'] = $data['title'];
        $this->article_model->sql_where = $data_where;
        $existdb = $this->article_model->get_one();
        if (empty($existdb) === false && $id != $existdb[$this->article_model->primary_key]) {
            $json["msg"] = self::$title_module . ' bestaat al!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $rsdb = $this->article_model->get_one_by_id($id);
        if (empty($rsdb) === false) {
            $this->article_model->edit($id, $data);
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . " is bijgewerkt!";
            $json["status"] = "good";
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function getPostdata()
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }
        $data = $this->article_model->get_postdata();
        return $data;
    }

    public function view($id = 0)
    {
        $data["rsdb"] = $this->article_model->get_one_by_id($id);
        if (empty($data["rsdb"]) === true) {
            redirect($this->controller_url);
        }

        $data["title"] = $data["rsdb"]["title"];
        $data["event_result_box"] = "";
        $this->article_model->update_hits($data["rsdb"]);
        $this->view_layout("view", $data);
    }
}
