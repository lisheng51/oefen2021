<?php

$path = explode(DIRECTORY_SEPARATOR, dirname(__FILE__));
$data["path"] = end($path);
$data["use_path"] = true;
$data["path_description"] = 'Artikel';
$data["arrPermission"] = [
    'Article' => [
        [
            'index' => 'Overzicht',
            'add' => 'Toevoegen',
        ]
    ],
    'Article_cate' => [
        [
            'index' => 'Overzicht catergorie ',
            'add' => 'Nieuw catergorie ',
        ]
    ]
];
