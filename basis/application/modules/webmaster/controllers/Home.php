<?php

class Home extends MX_Controller
{

    public function index()
    {
        $module = $this->router->module;
        redirect($module . '/' . $this->access_check_model->backPath . '/Home');
    }
}
