<div class="card">
    <div class="card-header"><?php echo $title ?></div>
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <?php echo $result; ?>
            </div>
        </div>
    </div>
</div>

<div class="card mt-3">
    <div class="card-header">libraries\helper</div>
    <div class="card-body">
        <div class="row">
            <?php foreach ($listdb as $class => $value) : ?>
                <div class="col-3">
                    <div class="card">
                        <div class="card-header"><?php echo $class ?></div>
                        <?php foreach ($value as $method => $description) : ?>
                            <div class="card-body">
                                <p><?php echo implode("<br>", $description) ?></p>
                                <p><?php echo $method ?></p>
                                <hr>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>