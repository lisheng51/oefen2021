function checkIsMobile() {
    if (navigator.userAgent.indexOf("Mobile") > 0) {
        return true;
    } else {
        return false;
    }
}


function ajax_inline_boolean(ajaxurl, showBox = true) {
    $("tbody td label").on('click', '.inline_boolean', function () {
        var currentEle = $(this);
        var editid = currentEle.data("edit-id");
        var field = currentEle.data("field");
        var value = currentEle.data('value');
        var newValue = value === 0 ? 1 : 0;
        var senddata = {
            editid: editid,
            field: field,
            type: 'boolean',
            fieldvalue: newValue,
            [csrf_token_name]: csrf_hash
        };

        $.ajax({
            url: ajaxurl,
            data: senddata,
            type: 'POST',
            dataType: 'json'
        }).done(function (json) {
            if (json.status !== 'good') {
                handle_info_box(json.status, json.msg);
            }

            if (showBox === true && json.status == 'good') {
                handle_info_box(json.status, json.msg);
            }
        }).always(function () {
            currentEle.data('value', newValue);
        }).fail(function (jqxhr) {
            message_ajax_fail_show(jqxhr);
        });
    });
}



function ajax_inline_edit(ajaxurl, showBox = true, inputsize = 25) {

    $("tbody td").on('dblclick', '.inline_edit', function (e) {
        e.preventDefault();
        e.stopPropagation();
        if ($('input.inline_edit_input').length > 0) return;

        var currentEle = $(this);
        var editid = currentEle.data("edit-id");
        var field = currentEle.data("field");
        var value = $(this).text();

        currentEle.html('<input size="' + inputsize + '" class="inline_edit_input" type="text" value="' + value + '" />');
        $("input.inline_edit_input").focus();
        $("input.inline_edit_input").keyup(function (event) {
            if (event.keyCode == 13) {
                var newValue = $("input.inline_edit_input").val().trim();
                if (value !== newValue) {
                    var senddata = {
                        editid: editid,
                        field: field,
                        fieldvalue: newValue,
                        [csrf_token_name]: csrf_hash
                    };
                    $.ajax({
                        url: ajaxurl,
                        data: senddata,
                        type: 'POST',
                        dataType: 'json'
                    }).done(function (json) {
                        if (json.status !== 'good') {
                            handle_info_box(json.status, json.msg);
                        }

                        if (showBox === true && json.status == 'good') {
                            handle_info_box(json.status, json.msg);
                        }

                        if (json.status == 'good') {
                            currentEle.text(newValue);
                        }
                    }).always(function () {

                    }).fail(function (jqxhr) {
                        message_ajax_fail_show(jqxhr);
                    });
                } else {
                    currentEle.text(newValue);
                }

            }

        });
    });
}

function ajax_sort(ajaxurl) {
    $('tbody#itemContainer').sortable({
        opacity: 0.6,
        revert: true,
        scroll: true,
        scrollSensitivity: 100,
        scrollSpeed: 100,
        items: "tr",
        handle: 'button#start_ajax_sort',
        cancel: '',
        cursor: 'move',
        helper: function (e, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });
            return ui;
        },
        start: function (e, ui) { },
        stop: function (e, ui) { },
        update: function (e, ui) {
            var ul = $(ui.item).closest('tbody#itemContainer');
            var index = 0;
            var toPost = {
                [csrf_token_name]: csrf_hash
            };
            ul.find('>tr').each(function () {
                index++;
                $(this).find('input').val(index);
                toPost[$(this).find('input').attr('name')] = index;
            });
            $.ajax({
                url: ajaxurl,
                data: toPost,
                type: 'POST',
                dataType: 'json'
            }).done(function (json) { }).always(function () {

            }).fail(function (jqxhr) {
                message_ajax_fail_show(jqxhr);
            });

        }
    });
}

function crop_user_image(aspectRatio) {
    $('a.user_pic_reset').click(function () {
        $('img#user_pic').attr("src", $(this).data("reset_value"));
    });

    var $image = $(".image-cropper > img");
    $("#modal_user_pic").on("shown.bs.modal", function () {
        $('img.toshow').attr("src", $('img#user_pic').attr("src"));
        $image.cropper({
            aspectRatio: aspectRatio,
            multiple: false,
            preview: ".img-preview",
        });
    });

    $('#modal_user_pic .rotate').click(function () {
        $image.cropper('rotate', 90)
    });

    $('#modal_user_pic .save').click(function () {
        var croppedCanvas;
        croppedCanvas = $image.cropper('getCroppedCanvas');
        var ckemptyimagepath = site_url + "asset/load/assets/img/0.png";
        if ($image.attr('src') === ckemptyimagepath) {
            handle_info_box('error', "Er is nog geen foto geselecteerd!");
        } else {
            $('img#user_pic').attr("src", croppedCanvas.toDataURL());
            $('input#user_pic').val(croppedCanvas.toDataURL());
            $('#modal_user_pic').modal('toggle');
        }
    });

    $("input#pic").change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function (e) {
                $image.cropper("reset", true).cropper("replace", e.target.result);
            };
        }
    });
}

function input_reportrange(select) {
    $(select).daterangepicker({
        locale: {
            direction: 'ltr',
            format: 'DD-MM-YYYY',
            separator: ' t/m ',
            applyLabel: 'OK',
            cancelLabel: 'Annuleer',
            weekLabel: 'W',
            customRangeLabel: 'Of anders van',
            daysOfWeek: [
                "Zo",
                "Ma",
                "Di",
                "Wo",
                "Do",
                "Vr",
                "Za"
            ],
            monthNames: [
                "Jan",
                "Feb",
                "Maa",
                "Apr",
                "Mei",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Okt",
                "Nov",
                "Dec"
            ],
            //daysOfWeek: moment.weekdaysMin(),
            //monthNames: moment.monthsShort(),
            firstDay: moment.localeData().firstDayOfWeek()
        },
        ranges: {
            'Vandaag': [moment(), moment()],
            'Gisteren': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Afgelopen 7 dagen': [moment().subtract(6, 'days'), moment()],
            'Laatste 30 dagen': [moment().subtract(29, 'days'), moment()],
            'Deze maand': [moment().startOf('month'), moment().endOf('month')],
            'Vorige maand': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Hele jaar': [moment().startOf('year'), moment().endOf('year')]
        }
    }, function (start, end) {
        $(select).val(start.format('DD-MM-YYYY') + ' - ' + end.format('DD-MM-YYYY'));
    });

    $(select).on('cancel.daterangepicker', function () {
        $(this).val('');
    });
    $(select).val("");
}

function input_date(select) {
    var hasSetVal = ($(select).val());
    $(select).daterangepicker({
        showDropdowns: true,
        showWeekNumbers: false,
        singleDatePicker: true,
        locale: {
            direction: 'ltr',
            format: 'DD-MM-YYYY',
            separator: ' t/m ',
            applyLabel: 'OK',
            cancelLabel: 'Annuleer',
            weekLabel: 'W',
            customRangeLabel: 'Of anders van',
            daysOfWeek: [
                "Zo",
                "Ma",
                "Di",
                "Wo",
                "Do",
                "Vr",
                "Za"
            ],
            monthNames: [
                "Jan",
                "Feb",
                "Maa",
                "Apr",
                "Mei",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Okt",
                "Nov",
                "Dec"
            ],
            //daysOfWeek: moment.weekdaysMin(),
            //monthNames: moment.monthsShort(),
            firstDay: moment.localeData().firstDayOfWeek()
        }
    }, function (start_date) {
        $(select).val(start_date.format('DD-MM-YYYY'));
    });
    $(select).val("");
    if (hasSetVal) {
        $(select).val(hasSetVal);
    }
    $(select).on('cancel.daterangepicker', function () {
        $(this).val('');
    });
}

function input_datewithtime(select) {
    var hasSetVal = ($(select).val());
    $(select).daterangepicker({
        //autoUpdateInput: true,
        singleDatePicker: true,
        timePicker: true,
        timePickerSeconds: true,
        timePicker24Hour: true,
        locale: {
            direction: 'ltr',
            format: 'DD-MM-YYYY HH:mm:ss',
            separator: ' t/m ',
            applyLabel: 'OK',
            cancelLabel: 'Annuleer',
            weekLabel: 'W',
            customRangeLabel: 'Of anders van',
            daysOfWeek: [
                "Zo",
                "Ma",
                "Di",
                "Wo",
                "Do",
                "Vr",
                "Za"
            ],
            monthNames: [
                "Jan",
                "Feb",
                "Maa",
                "Apr",
                "Mei",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Okt",
                "Nov",
                "Dec"
            ],
            //daysOfWeek: moment.weekdaysMin(),
            //monthNames: moment.monthsShort(),
            firstDay: moment.localeData().firstDayOfWeek()
        }
    }, function (chosen_date) {
        $(select).val(chosen_date.format('DD-MM-YYYY HH:mm:ss'));
    });

    $(select).val("");
    if (hasSetVal) {
        $(select).val(hasSetVal);
    }

    $(select).on('cancel.daterangepicker', function () {
        $(this).val('');
    });
}

function button_reset() {
    $("button.reset").click(function () {
        $("form#form_search").find("select, input[type=text], input[type=email]").val("");
        $("form#form_search").find('select').selectpicker('refresh');
        $('input:checkbox').prop('checked', false);
        ajax_form_search($("form#form_search"));
    });
}

function ajax_search_filter(select, selectname) {
    $("form#form_search select[name='" + selectname + "']").val($(select).data("search_data"));
    $("form#form_search").find("select[name='" + selectname + "']").selectpicker('refresh');
    ajax_form_search($("form#form_search"));
}

function handle_info_box(status, msg) {
    var title = "Informatie";
    switch (status) {
        case 'error':
            title = "<span class='text-danger'>Foutmelding</span>";
            Swal.fire({
                title: title,
                icon: 'error',
                html: msg,
                showCancelButton: false,
                cancelButtonText: "Nee",
                confirmButtonText: "OK",
                timer: 3000
            });
            break;
        case 'good':
            title = "<span class='text-primary'>Succes</span>";
            Swal.fire({
                title: title,
                icon: 'success',
                html: msg,
                showCancelButton: false,
                cancelButtonText: "Nee",
                confirmButtonText: "OK",
                timer: 3000
            });
            break;
        case 'waiting':
            title = "Even geduld aub...";
            break;
        case 'info':
            title = "<span class='text-info'>Melding</span>";
            Swal.fire({
                title: title,
                icon: 'info',
                html: msg,
                showCancelButton: false,
                cancelButtonText: "Nee",
                confirmButtonText: "OK"
            });
            break;
        default:
            break;
    }
}

function handle_delete_box() {
    $("div#ajax_search_content").on('click', 'tr button.delButton', function (e) {
        var del_id = $(this).data('search_data');
        var del_url = $(this).data('del_link');
        Swal.fire({
            title: 'Bevestig uw keuze',
            icon: 'question',
            html: "Weet u zeker dat u deze wilt verwijderen?",
            showCancelButton: true,
            cancelButtonText: "Nee",
            confirmButtonText: "Ja"
        }).then((result) => {
            if (result.value) {
                var senddata = { del_id: del_id, [csrf_token_name]: csrf_hash };
                $.ajax({
                    url: del_url,
                    data: senddata,
                    type: 'POST',
                    dataType: 'json'
                }).done(function (json) {
                    if (json.status === 'good') {
                        $("#" + del_id + "").fadeOut('slow', function () {
                            $(this).remove();
                            var countnow = $("span.totalcount").text();
                            $('span.totalcount').text(countnow - 1);
                        });
                    }
                    handle_info_box(json.status, json.msg);
                }).always(function () {

                }).fail(function (jqxhr) {
                    message_ajax_fail_show(jqxhr);
                });
            }
        });
    });
}

function ajax_form_search(form_id, ajaxurlcustomer) {
    var checkIfAction = "yes";
    if (typeof form_id.data('app-form-action') !== 'undefined') {
        checkIfAction = form_id.data('app-form-action');
    }

    if (checkIfAction === "yes") {
        var senddata = new FormData(form_id[0]);
        if ($("select[name=page_limit]").length) {
            senddata.append('page_limit', $("select[name=page_limit]").val() ?? 0);
        }
        if ($("select[name=sql_orderby_field]").length) {
            senddata.append('sql_orderby_field', $("select[name=sql_orderby_field]").val() ?? "");
        }
        var submit_button = form_id.find('button[type=submit]');
        var ajaxurl = window.location.href;

        if (typeof form_id.data('app-target') !== 'undefined') {
            ajaxurl = form_id.data('app-target');
        }
        if (ajaxurlcustomer) {
            ajaxurl = ajaxurlcustomer;
        }
        ajax_search(ajaxurl, senddata, submit_button);
    }
}

function ajax_pagination() {
    $("div#ajax_search_content").on('click', '.pagination a', function (e) {
        e.preventDefault();
        var ajaxurl = $(this).get(0).href;
        ajax_form_search($("form#form_search"), ajaxurl)
    });

    $("select[name=sql_orderby_field]").change(function (e) {
        e.preventDefault();
        ajax_form_search($("form#form_search"))
    });

    $("select[name=page_limit]").change(function (e) {
        e.preventDefault();
        ajax_form_search($("form#form_search"))
    });
}

function axios_search(ajaxurl, item, submit_button) {
    var form_data = new FormData();
    form_data.append([csrf_token_name], csrf_hash);
    for (var section in item) {
        if (item[section] instanceof Array) {
            item[section].forEach(value => form_data.append(section + '[]', value));
        } else {
            form_data.append(section, item[section]);
        }
    }

    ajax_search(ajaxurl, form_data, submit_button);
}

function ajax_search(ajaxurl, senddata, submit_button) {
    const submit_button_text = submit_button.html();
    axios({
        method: 'post',
        url: ajaxurl,
        data: senddata,
        headers: {
            'Content-Type': false,
            'processData': false,
        },
        onUploadProgress: (progressEvent) => {
            submit_button.attr("disabled", "disabled");
            submit_button.html('<i class="fa fa-spinner fa-pulse"></i> ');
        }
    }).then((response) => {
        const headerval = response.headers['content-disposition'];
        if (typeof headerval !== 'undefined') {
            const filename = headerval.split(';')[1].split('=')[1].replace('"', '').replace('"', '');
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', filename);
            document.body.appendChild(link);
            link.click();
            link.remove();
        }

        const json = response.data;
        if (json.type_done) {
            switch (json.type_done) {
                case 'redirect':
                    if (json.redirect_url) {
                        window.location.href = json.redirect_url;
                    }
                    break;
                default:
                    break;
            }
        }
        if (json.result) {
            $("#ajax_search_content").html(json.result);
        } else {
            handle_info_box(json.status, json.msg);
        }

        event_result_box(json.status, json.msg);
    }).catch((error) => {
        const jqxhr = error.response;
        if (typeof jqxhr !== 'undefined') {
            message_ajax_fail_show(jqxhr);
        }
        console.log(error);
    }).then((response) => {
        submit_button.html(submit_button_text);
        submit_button.removeAttr("disabled");
    });
}

function event_result_box(status, msg) {
    var title;
    switch (status) {
        case 'error':
            title = "Waarschuwing";
            break;
        case 'good':
            title = "Success";
            break;
        case 'waiting':
            title = "Even geduld aub...";
            break;
        default:
            break;
    }

    var oldheader = $('#event_result_box').find('.card-heading').html();
    var oldbody = $('#event_result_box').find('.card-body').html();

    $('#event_result_box').find('.card-heading').html(title);
    $('#event_result_box').find('.card-body').html(msg);

    if ($.trim($('#event_result_box').find('.card-body').html()).length) {
        setTimeout(function () {
            $('#event_result_box').find('.card-heading').html(oldheader);
            $('#event_result_box').find('.card-body').html(oldbody);
        }, 3000);
    }
}

function fail_login_data() {
    var ajaxurl = site_url + "ajax/login/fail_check";
    setInterval(function () {
        $.getJSON(ajaxurl, function (json) {
            if (json.status == 'error') {
                window.location.href = json.redirect_url;
            }
        });
    }, 5000);
}

function setup_tinymce(select, height = 300, content_style = "") {
    tinymce.init({
        selector: select,
        language_url: site_url + 'node_modules/tinymce-i18n/langs/nl.js', // site absolute URL
        language: 'nl',
        branding: false,
        menubar: false,
        paste_data_images: true,
        height: height,
        content_style: content_style,
        plugins: 'image code',
        relative_urls: false,
        remove_script_host: false,
        statusbar: false,
        //document_base_url: site_url,
        // toolbar: 'undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | image code'
        toolbar: 'undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | code'
        //toolbar: 'bold italic underline alignleft aligncenter alignright alignjustify bullist'
    });
}

function refreshTinymceFileUrl(base64, name, user_id = 0) {
    let newpath = "";
    let senddata = {
        base64: base64,
        name: name,
        user_id: user_id,
    };
    $.ajax({
        async: false,
        url: site_url + 'ajax/Upload/tinymce',
        data: senddata,
        type: 'POST',
        dataType: 'json'
    }).done(function (json) {
        newpath = json.result;
    }).always(function () { }).fail(function (jqxhr) {
        message_ajax_fail_show(jqxhr);
    });

    return newpath;
}

function setup_tinymce_noxss_clean(selector = '.tinymce_noxss_clean', height = 600, user_id = 0, content_style = "") {
    tinymce.init({
        selector: selector,
        language_url: site_url + 'node_modules/tinymce-i18n/langs/nl.js', // site absolute URL
        language: 'nl',
        branding: false,
        menubar: false,
        toolbar: 'undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect | image code link',
        height: height,
        content_style: content_style,
        plugins: 'image paste code link',
        relative_urls: false,
        remove_script_host: false,
        paste_as_text: true,
        image_title: true,
        statusbar: false,
        file_picker_types: 'image',
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            //input.setAttribute('accept', '.png, .jpg, .jpeg, .gif, .bmp');
            input.onchange = function () {
                var file = this.files[0];
                var reader = new FileReader();
                reader.onload = function () {
                    let base64 = reader.result.split(',')[1];
                    if (meta.filetype == 'image') {
                        let newPath = refreshTinymceFileUrl(base64, file.name, user_id);
                        cb(newPath, {
                            title: file.name
                        });
                    }
                };
                reader.readAsDataURL(file);
            };
            input.click();
        }
    });
}

function message_ajax_fail_show(jqxhr) {
    var htmlString;
    if (message_ajax_fail) {
        htmlString = message_ajax_fail;
    } else {
        var str = jqxhr.responseText ?? jqxhr.data;
        htmlString = str.replace(/<style\b[^<>]*>[\s\S]*?<\/style\s*>/gi, '');
    }
    var title = "<span class='text-danger'>Foutmelding - " + jqxhr.status + " " + jqxhr.statusText + "</span>";
    Swal.fire({
        title: title,
        icon: 'error',
        html: str,
        showCancelButton: false,
        cancelButtonText: "Nee",
        confirmButtonText: "OK"
    });
}


function activaTab(tab) {
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    $('.navbar-nav a[href="#' + tab + '"]').tab('show');
}

function HrefPermissionCk(select) {
    $(select).click(function () {
        var redirectUrl = $(this).attr('href');
        var pathArray = redirectUrl.split(site_url);
        var pathurl = pathArray[1];
        if (pathurl) {
            $.ajax({
                url: site_url + 'ajax/Permission/checkForUser',
                data: { pathurl: pathurl },
                type: 'POST',
                dataType: 'json'
            }).done(function (json) {
                if (json.type_done) {
                    switch (json.type_done) {
                        case 'redirect':
                            if (json.redirect_url) {
                                window.location.href = json.redirect_url;
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (json.status === 'error') {
                    handle_info_box(json.status, json.msg);
                } else {
                    window.location.href = redirectUrl;
                }
            }).fail(function (jqxhr) {
                message_ajax_fail_show(jqxhr);
            });
        }
        return false;
    });

}