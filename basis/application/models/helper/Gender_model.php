<?php

class Gender_model extends CI_Model
{

    public $primary_key = "gender_id";
    public $id_1 = 1;
    public $id_2 = 2;
    public $id_3 = 3;
    public $id_4 = 4;

    public function listDB(): array
    {
        $data[] = $this->id_1();
        $data[] = $this->id_2();
        $data[] = $this->id_3();
        $data[] = $this->id_4();
        return $data;
    }

    private function id_1(string $key = '')
    {
        $data[$this->primary_key] = $this->id_1;
        $data['name'] = '------';
        $data['href'] = "";
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function id_2(string $key = '')
    {
        $data[$this->primary_key] = $this->id_2;
        $data['name'] = 'Vrouw';
        $data['href'] = 'Mevrouw';
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function id_3(string $key = '')
    {
        $data[$this->primary_key] = $this->id_3;
        $data['name'] = 'Man';
        $data['href'] = 'Meneer';
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function id_4(string $key = '')
    {
        $data[$this->primary_key] = $this->id_4;
        $data['name'] = 'Neutraal';
        $data['href'] = '';
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    public function fetchData(int $id = 0, string $field = '')
    {
        switch ($id) {
            case $this->id_1:
                $value = $this->id_1($field);
                break;
            case $this->id_2:
                $value = $this->id_2($field);
                break;
            case $this->id_3:
                $value = $this->id_3($field);
                break;
            case $this->id_4:
                $value = $this->id_4($field);
                break;
            default:
                $value = null;
                break;
        }
        return $value;
    }

    public function fetchName(int $id = 0, string $field = 'name')
    {
        return $this->fetchData($id, $field);
    }

    public function select(int $id = 0, string $name = ''): string
    {
        $listdb = $this->listDB();
        $select_name = empty($name) === false ? $name : $this->primary_key;
        $select = '<select name=' . $select_name . ' class="form-control selectpicker">';
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= "<option value={$rs[$this->primary_key]} $ckk >{$rs["name"]}</option>";
        }
        $select .= '</select>';
        return $select;
    }

    public function fetchGenderId(string $value = "")
    {
        switch ($value) {
            case 'male':
                return 3;
            case 'female':
                return 2;
            default:
                return 1;
        }
    }
}
