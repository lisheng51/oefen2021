<?php

class Barcode_model extends CI_Model
{

    public function app2web(string $token = ''): string
    {
        if (empty($token) === true) {
            return $this->emptyCode();
        }
        $data['token'] = $token;
        $data['expires'] = date('Y-m-d H:i:s', strtotime('+15 minutes'));
        $data['function'] = __FUNCTION__;
        return $this->makeCode($data);
    }

    public function web2app(): string
    {
        if ($this->login_model->user_id() <= 0) {
            return $this->emptyCode();
        }
        $data['user_id'] = $this->login_model->user_id();
        $data['function'] = __FUNCTION__;
        return $this->makeCode($data);
    }

    public function showCodeInApp(int $userId = 0): string
    {
        if ($userId <= 0) {
            return $this->emptyCode();
        }
        $data['user_id'] = $userId;
        $data['function'] = 'web2app';
        return $this->makeCode($data);
    }

    public function makeCode(array $data = [], bool $onlyCode = false): string
    {
        $valuestring = json_encode($data);
        $tokenString = $this->encryption->encrypt($valuestring);
        $code = rawurlencode($tokenString);
        if ($onlyCode === true) {
            return $code;
        }
        $generator = new chillerlan\QRCode\QRCode();
        return $generator->render($code);
    }

    public function string2Data(string $encodeString = "", bool $asArray = true)
    {
        $valuestring = rawurldecode($encodeString);
        $tokenString = $this->encryption->decrypt($valuestring);
        if ($tokenString === false) {
            return [];
        }
        $arrTokenValue = json_decode($tokenString, $asArray);
        return $arrTokenValue;
    }

    private function emptyCode(): string
    {
        return sys_asset_url('img/0.png');
    }

    public function oneD(string $content = ""): string
    {
        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
        $base64 = 'data:image/png;base64,' . base64_encode($generator->getBarcode($content, $generator::TYPE_CODE_128));
        return $base64;
    }
}
