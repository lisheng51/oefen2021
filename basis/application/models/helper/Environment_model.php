<?php

class Environment_model extends CI_Model
{

    public $primary_key = "environment_id";
    public $id_1 = 1;
    public $id_2 = 2;
    public $id_3 = 3;
    public $allowIpsId_1 = "127.0.0.1,::1";
    public $allowIpsId_2 = "";
    public $allowIpsId_3 = "";

    public function select(int $id = 0, bool $with_empty = false): string
    {
        $listdb = $this->listDB();
        $select = '<select name=' . $this->primary_key . ' class="form-control selectpicker">';
        $select .= $with_empty === true ? "<option value='' >------</option>" : "";
        foreach ($listdb as $rs) {
            $ckk = $id == $rs[$this->primary_key] ? "selected" : '';
            $select .= "<option value={$rs[$this->primary_key]} $ckk >{$rs["name"]}</option>";
        }
        $select .= '</select>';
        return $select;
    }

    public function listDB(): array
    {
        $data[] = $this->id_1();
        $data[] = $this->id_2();
        $data[] = $this->id_3();
        return $data;
    }

    private function id_1(string $key = '')
    {
        $data[$this->primary_key] = $this->id_1;
        $data['name'] = 'Development';
        $data['description'] = null;
        $data['allow_ips'] = $this->allowIpsId_1;
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function id_2(string $key = '')
    {
        $data[$this->primary_key] = $this->id_2;
        $data['name'] = 'Productie';
        $data['description'] = null;
        $data['allow_ips'] = $this->allowIpsId_2;
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    private function id_3(string $key = '')
    {
        $data[$this->primary_key] = $this->id_3;
        $data['name'] = 'Test';
        $data['description'] = null;
        $data['allow_ips'] = $this->allowIpsId_3;
        if (empty($key) === false) {
            return $data[$key];
        }
        return $data;
    }

    public function fetch_field(int $id = 0, string $field = '')
    {
        switch ($id) {
            case $this->id_1:
                $value = $this->id_1($field);
                break;
            case $this->id_2:
                $value = $this->id_2($field);
                break;
            case $this->id_3:
                $value = $this->id_3($field);
                break;
            default:
                $value = null;
                break;
        }
        return $value;
    }

    public function fetchName(int $id = 0)
    {
        return $this->fetch_field($id, 'name');
    }

    public function fetchAllowIps(int $id = 0)
    {
        return $this->fetch_field($id, 'allow_ips');
    }
}
