<?php

class Login_model extends CI_Model
{

    public $table = "login";
    private $webapp_access_code = null;
    public $login_user_data = [];
    public $primary_key = "user_id";
    public $session_key = "login";

    public function user_id()
    {
        $session_data = $this->session->userdata($this->session_key);
        if (isset($session_data[$this->primary_key]) === true) {
            return intval($session_data[$this->primary_key]);
        }
        return 0;
    }

    public function redirect_url()
    {
        $session_data = $this->session->userdata($this->session_key);
        if (empty($session_data['redirect_url']) === false) {
            return $session_data['redirect_url'];
        }
        return $this->access_check_model->backPath . '/home';
    }

    public function is_double_login()
    {
        $arr_ck = $this->get_logindata();
        if (empty($arr_ck) === true) {
            return true;
        }
        $user_data = $arr_ck["ip_address"] . $arr_ck["browser"] . $arr_ck["platform"];
        $check_data = $this->input->ip_address() . $this->agent->browser() . $this->agent->platform();
        if ($user_data === $check_data) {
            return false;
        }
        return true;
    }

    private function get_logindata()
    {
        $user_id = $this->user_id();
        $query = $this->db->from($this->table)->where($this->primary_key, $user_id)->limit(1)->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return [];
    }

    public function get_one(string $username = "", string $password = "")
    {
        if (empty($username) === true || empty($password) === true) {
            return [];
        }
        $rs = $this->get_one_by_username_email($username);
        if (empty($rs) === true || $rs["is_active"] == 0 || $rs["is_del"] == 1) {
            return array();
        }

        if (password_verify($password, $rs["password"]) === true) {
            return $rs;
        }
        return [];
    }

    public function check2FA(array $arr_rs = [], string $oneCode = "")
    {
        if (empty($arr_rs) === true) {
            return false;
        }

        if ($arr_rs["with_2fa"] > 0 && empty($arr_rs["2fa_secret"]) === false) {
            require_once APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'GoogleAuthenticator.php';
            $ga = new libraries\GoogleAuthenticator();
            $checkResult = $ga->verifyCode($arr_rs["2fa_secret"], $oneCode, 2);    // 2 = 2*30sec clock tolerance
            return $checkResult;
        }

        return true;
    }

    public function show2FAQrcode($string_2fa_secret = null)
    {
        require_once APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'GoogleAuthenticator.php';
        $ga = new libraries\GoogleAuthenticator();
        $secret = $ga->createSecret();
        if (empty($string_2fa_secret) === false) {
            $secret = $string_2fa_secret;
        }

        $qrCodeUrl = $ga->getQRCodeGoogleUrl(site_url(), $secret, c_key('webapp_title'));
        $this->session->set_userdata('2fa_secret', $secret);
        return '<img src="' . (new chillerlan\QRCode\QRCode)->render($qrCodeUrl) . '" />';
    }

    public function check2FAStatus(int $uid = 0, string $oneCode = "")
    {
        if (empty($oneCode) === false && $uid > 0) {
            require_once APPPATH . 'libraries' . DIRECTORY_SEPARATOR . 'GoogleAuthenticator.php';
            $ga = new libraries\GoogleAuthenticator();
            $secret = $this->session->userdata('2fa_secret');
            $checkResult = $ga->verifyCode($secret, $oneCode, 2);
            return $checkResult;
        }
    }

    public function get_one_by_username_email(string $username = "")
    {
        if (empty($username) === true) {
            return [];
        }
        $query = $this->db->from($this->table)
            ->join($this->user_model->table, $this->primary_key)
            ->where("username", $username)
            ->limit(1)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return [];
    }

    public function check_data(string $username = "", string $password = "")
    {
        if (empty($username) === true || empty($password) === true) {
            return false;
        }
        $arr_rs = $this->get_one($username, $password);
        if (empty($arr_rs) === true) {
            return false;
        }

        if (empty($arr_rs["access_code_date"]) === false && date("Y-m-d H:i:s") > $arr_rs["access_code_date"]) {
            return false;
        }

        if (empty($arr_rs["access_code"]) === false && $this->webapp_access_code !== $arr_rs["access_code"]) {
            return false;
        }
        $this->login_user_data = $arr_rs;
        return true;
    }

    public function update_data(int $user_id = 0)
    {
        if ($user_id <= 0) {
            return;
        }
        $data["ip_address"] = $this->input->ip_address();
        $data["browser"] = $this->agent->browser();
        $data["platform"] = $this->agent->platform();
        $data["is_online"] = 1;
        $data["date"] = date('Y-m-d H:i:s');
        $data["access_code"] = null;
        $data["access_code_date"] = null;
        $this->edit($user_id, $data);
    }

    public function logout()
    {
        if ($this->user_id() <= 0) {
            return;
        }
        add_app_log("Gebruiker is uitgelogd");
        $data["is_online"] = 0;
        $this->edit($this->user_id(), $data);
        $this->session->sess_destroy();
    }

    public function add($data = [])
    {
        if (empty($data) === true) {
            return;
        }
        $this->db->insert($this->table, $data);
    }

    public function edit(int $uid = 0, array $data = [])
    {
        if ($uid <= 0 || empty($data) === true) {
            return;
        }
        $this->db->where($this->primary_key, $uid)->update($this->table, $data);
    }

    public function getByField(string $field = 'username', string $fieldValue = '')
    {
        if (empty($fieldValue) === true) {
            return [];
        }
        $query = $this->db->from($this->table)
            ->join($this->user_model->table, $this->primary_key)
            ->where($field, $fieldValue)
            ->limit(1)
            ->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        }
        return [];
    }

    public function add_user_session()
    {
        $arr_rs = $this->login_user_data;
        if (empty($arr_rs) === true) {
            return;
        }
        $sess_array[$this->user_model->primary_key] = $arr_rs[$this->user_model->primary_key];
        $sess_array['redirect_url'] = $arr_rs["redirect_url"];
        $this->session->set_userdata($this->session_key, $sess_array);
        $this->update_data($arr_rs[$this->primary_key]);
        $this->login_history_model->insert($arr_rs[$this->primary_key]);
        add_app_log("Gebruiker is ingelogd");
    }

    public function set_access_code(string $code = "")
    {
        if (empty($code) === false) {
            $this->webapp_access_code = $code;
        }
    }

    public function update_access_code(int $user_id = 0)
    {
        if ($user_id <= 0) {
            return;
        }
        $data["access_code"] = F_string::random(6);
        $data["access_code_date"] = date('Y-m-d H:i:s', strtotime('+15 minutes'));
        $this->edit($user_id, $data);
        return $data;
    }

    public function check_password_date(string $password_date = "")
    {
        if (intval(c_key('webapp_ck_pass_unuse_day')) > 0) {
            $webapp_ck_pass_unuse_day = c_key('webapp_ck_pass_unuse_day');
            $webapp_ck_pass_notify_day = c_key("webapp_ck_pass_notify_day") > 0 ? c_key("webapp_ck_pass_notify_day") : 10;
            $day_msg = $webapp_ck_pass_unuse_day - $webapp_ck_pass_notify_day;
            $days_ago_no_login = date('Y-m-d H:i:s', strtotime("-$webapp_ck_pass_unuse_day days"));
            $days_ago_msg = date('Y-m-d H:i:s', strtotime("-$day_msg days"));
            if ($password_date <= $days_ago_msg || $password_date <= $days_ago_no_login) {
                return false;
            }
        }

        return true;
    }

    public function update_password_reset_date(int $user_id = 0)
    {
        $data["password_reset_date"] = date('Y-m-d H:i:s');
        if ($user_id > 0) {
            $webapp_ck_pass_reset_hour = c_key("webapp_ck_pass_reset_hour") > 0 ? c_key("webapp_ck_pass_reset_hour") : 8;
            $much_strtotime = '+' . $webapp_ck_pass_reset_hour . ' hours';
            $data["password_reset_date"] = date('Y-m-d H:i:s', strtotime($much_strtotime));
            $this->edit($user_id, $data);
        }
        return $data;
    }
}
