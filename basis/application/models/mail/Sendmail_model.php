<?php

class Sendmail_model extends Mail_model
{

    public $table = "send_mail";
    public $primary_key = "mail_id";
    public $sql_where = [];
    public $sql_where_in = [];
    public $sql_like = [];
    public $sql_order_by = [];
    public $select_order_by = [];

    public $setFromEmail = "";
    public $setReplyTo = [];
    public $setCc = [];
    public $setBcc = [];
    public $setFile = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'mail_id#desc' => 'ID (aflopend)',
            'subject#desc' => 'Onderwerp (aflopend)',
            'subject#asc' => 'Onderwerp (oplopend)',
            'send_date#desc' => 'Verzonden (aflopend)',
            'send_date#asc' => 'Verzonden (oplopend)',
        ];
    }

    public function setSqlWhere(array $whereSet = [])
    {
        $newArray = [];
        foreach ($whereSet as $array) {
            if (is_array($array)) {
                foreach ($array as $k => $v) {
                    if (!is_null($v) && $v !== '') {
                        $newArray[$k] = $v === 'value_is_null' ? null : $v;
                    }
                }
            }
        }
        $this->sql_where = $newArray;
    }

    public function preview($message = null, $subject = 'Voorbeeld', string $mail_to_address = "")
    {
        $data["content"] = $message;
        $data["title"] = $subject;
        return $this->add_send($data, $mail_to_address);
    }

    public function message_copie($message_id = 0, $user_db = [])
    {
        if (empty($user_db) === true || $message_id <= 0) {
            return false;
        }
        $emailaddress = $user_db["emailaddress"];

        $configValue = $this->mail_template_model->getByMethod(__FUNCTION__);
        $body = $configValue[__FUNCTION__ . '_body'];
        $subject = $configValue[__FUNCTION__ . '_subject'];

        $body = $this->mail_template_model->replace_body($body, $user_db);

        $data_button["button_url"] = login_url('?redirect_url=' . site_url($this->access_check_model->backPath . "/message/view/$message_id"));
        $data_button["button_text"] = "Hier";
        $button = $this->load->view($this->button_template, $data_button, true);
        $body = str_replace('{sys_button}', $button, $body);

        $data["title"] = $subject;
        $data["content"] = $body;
        $data["app_log"] = "message_copie gestuurd naar gebruiker";
        $data["app_log_user_id"] = $user_db["user_id"];
        return $this->add_send($data, $emailaddress);
    }

    public function user_active_confirm($user_db = [])
    {
        if (empty($user_db) === true) {
            return false;
        }
        $emailaddress = $user_db["emailaddress"];
        $configValue = $this->mail_template_model->getByMethod(__FUNCTION__);
        $body = $configValue[__FUNCTION__ . '_body'];
        $subject = $configValue[__FUNCTION__ . '_subject'];
        $body = $this->mail_template_model->replace_body($body, $user_db);

        $data_button["button_url"] = login_url();
        $data_button["button_text"] = "Hier";
        $button = $this->load->view($this->button_template, $data_button, true);
        $body = str_replace('{sys_button}', $button, $body);

        $data["title"] = $subject;
        $data["content"] = $body;
        $data["app_log"] = "user_active_confirm gestuurd naar nieuwe gebruiker";
        $data["app_log_user_id"] = $user_db["user_id"];
        return $this->add_send($data, $emailaddress);
    }

    public function password_reset($user_db = [])
    {
        if (empty($user_db) === true) {
            return false;
        }

        $emailaddress = $user_db["emailaddress"];
        $arr_reset = $this->login_model->update_password_reset_date($user_db["user_id"]);
        $maxTimeStampText = date_format(date_create($arr_reset["password_reset_date"]), 'd-m-Y H:i:s');
        $encrypted_string = $this->encryption->encrypt($user_db["user_id"] . "#_#" . $emailaddress);
        $hashkey = rawurlencode($encrypted_string);

        $configValue = $this->mail_template_model->getByMethod(__FUNCTION__);
        $body = $configValue[__FUNCTION__ . '_body'];
        $subject = $configValue[__FUNCTION__ . '_subject'];

        $body = $this->mail_template_model->replace_body($body, $user_db);
        $body = str_replace('{sys_reset_time}', $maxTimeStampText, $body);

        $data_button["button_url"] = site_url(ENVIRONMENT_ACCESS_URL . "/password_reset/reset?hashkey=$hashkey");
        $data_button["button_text"] = "Wachtwoord instellen";
        $button = $this->load->view($this->button_template, $data_button, true);

        $body = str_replace('{sys_button}', $button, $body);

        $data["title"] = $subject;
        $data["content"] = $body;
        $data["app_log"] = "password_reset gestuurd naar nieuwe gebruiker";
        $data["app_log_user_id"] = $user_db["user_id"];
        return $this->add_send($data, $emailaddress);
    }

    public function access_code_login($user_db = [], $password = null, $is_demo_preview = false)
    {
        if (empty($user_db) === true) {
            return false;
        }
        $arr_acces = $this->login_model->update_access_code($user_db["user_id"]);
        $encrypted_string = $this->encryption->encrypt($user_db["user_id"] . "#_#" . $arr_acces["access_code"]);
        $hashkey = rawurlencode($encrypted_string);

        if (ENVIRONMENT === 'development') {
            $this->telegram_model->log('Toegangscode: ' . $arr_acces["access_code"]);
        }
        $emailaddress = $user_db["emailaddress"];
        $configValue = $this->mail_template_model->getByMethod(__FUNCTION__);
        $body = $configValue[__FUNCTION__ . '_body'];
        $subject = $configValue[__FUNCTION__ . '_subject'];

        $body = $this->mail_template_model->replace_body($body, $user_db);
        $body = str_replace('{sys_access_code}', $arr_acces["access_code"], $body);
        $body = str_replace('{sys_access_code_date}', $arr_acces["access_code_date"], $body);

        $data_button["button_url"] = login_url();
        $data_button["button_text"] = "Inloggen";

        if (empty($password) === false) {
            $data_button["button_url"] = login_url("/direct?hashkey=$hashkey");
            $data_button["button_text"] = "Of direct inloggen";
        }

        $button = $this->load->view($this->button_template, $data_button, true);
        $body = str_replace('{sys_button}', $button, $body);

        $data["title"] = $subject;
        $data["content"] = $body;
        $data["app_log"] = "access_code_login gestuurd naar nieuwe gebruiker";
        $data["app_log_user_id"] = $user_db["user_id"];

        if ($is_demo_preview === true) {
            $this->login_model->update_data($user_db["user_id"]);
        }
        return $this->add_send($data, $emailaddress);
    }

    public function edit_email_code($user_db = [], $emailaddress = null)
    {
        if (empty($user_db) === true || empty($emailaddress) === true) {
            return false;
        }
        $code = F_string::random(6);
        $this->session->set_userdata('edit_email_code', $code);
        $old_email = $user_db["emailaddress"];

        $configValue = $this->mail_template_model->getByMethod(__FUNCTION__);
        $body = $configValue[__FUNCTION__ . '_body'];
        $subject = $configValue[__FUNCTION__ . '_subject'];

        $body = $this->mail_template_model->replace_body($body, $user_db);
        $body = str_replace('{sys_code}', $code, $body);
        $body = str_replace('{sys_old_email}', $old_email, $body);
        $body = str_replace('{sys_new_email}', $emailaddress, $body);

        $data["title"] = $subject;
        $data["content"] = $body;

        $data["app_log"] = "edit_email_code gestuurd naar nieuwe gebruiker";
        $data["app_log_user_id"] = $user_db["user_id"];
        return $this->add_send($data, $emailaddress);
    }

    public function user_register($user_db = [])
    {
        if (empty($user_db) === true) {
            return false;
        }
        $emailaddress = $user_db["emailaddress"];

        $configValue = $this->mail_template_model->getByMethod(__FUNCTION__);
        $body = $configValue[__FUNCTION__ . '_body'];
        $subject = $configValue[__FUNCTION__ . '_subject'];

        $body = $this->mail_template_model->replace_body($body, $user_db);

        $data["title"] = $subject;
        $data["content"] = $body;
        $data["app_log"] = "user_register gestuurd naar nieuwe gebruiker";
        $data["app_log_user_id"] = $user_db["user_id"];
        return $this->add_send($data, $emailaddress);
    }

    public function user_active($user_db = [])
    {
        if (empty($user_db) === true) {
            return false;
        }
        $emailaddress = $user_db["emailaddress"];
        $encrypted_string = $this->encryption->encrypt($user_db["user_id"] . "#_#" . $emailaddress);
        $hashkey = rawurlencode($encrypted_string);

        $configValue = $this->mail_template_model->getByMethod(__FUNCTION__);
        $body = $configValue[__FUNCTION__ . '_body'];
        $subject = $configValue[__FUNCTION__ . '_subject'];

        $body = $this->mail_template_model->replace_body($body, $user_db);

        $data_button["button_url"] = login_url("/active?hashkey=$hashkey");
        $data_button["button_text"] = "Account activeren";
        $button = $this->load->view($this->button_template, $data_button, true);

        $body = str_replace('{sys_button}', $button, $body);

        $data["title"] = $subject;
        $data["content"] = $body;
        $data["app_log"] = "user_active gestuurd naar nieuwe gebruiker";
        $data["app_log_user_id"] = $user_db["user_id"];
        return $this->add_send($data, $emailaddress);
    }

    public function add($data = [], $emailaddress = null, $attach = null, $toMessage = true): bool
    {
        $insert_data = $this->get_postdata($data, $emailaddress, $attach);
        if ($toMessage === true) {
            $dataMessage["content"] = $data["content"];
            $dataMessage["title"] = $data["title"];
            $dataMessage["to_user_id"] = $data["app_log_user_id"];
            $dataMessage["from_user_id"] = 0;
            $this->message_model->add($dataMessage);
        }

        return $this->db->insert($this->table, $insert_data);
    }

    public function add_send(array $data = [], $emailaddress = null, $attach = null): bool
    {
        $insert_data = $this->get_postdata($data, $emailaddress, $attach);
        $this->db->insert($this->table, $insert_data);
        $mailId = $this->db->insert_id();
        if ($mailId > 0) {
            $this->send_mail_file_model->upload($mailId, $this->setFile);
            $insert_data[$this->primary_key] = $mailId;
            $sendstatus = $this->send_action($insert_data);
            if ($sendstatus === true) {
                $dataedit["is_send"] = 1;
                $dataedit["send_date"] = date('Y-m-d H:i:s');
                $this->edit($mailId, $dataedit);
            }
            return true;
        }
        return false;
    }

    public function get_postdata(array $data = [], $emailaddress = null, $attach = null)
    {
        $insert["from_email"] = empty($this->setFromEmail) === false ? $this->setFromEmail : c_key('webapp_smtp_user');
        $insert["to_email"] = $emailaddress;
        $insert["subject"] = stripslashes($data["title"]);
        $insert["event_value"] = time();
        $insert["reply_to_json"] = json_encode($this->setReplyTo);
        $insert["cc_json"] = json_encode($this->setCc);
        $insert["bcc_json"] = json_encode($this->setBcc);
        $message["content"] = $data["content"];
        $message["title"] = $data["title"];

        $data_button["button_url"] = $this->create_event($insert, 'sys');
        $data_button["button_text"] = "Bekijk de web versie";
        $button = $this->load->view($this->button_template, $data_button, true);
        $message["webversion_button"] = intval(c_key('webapp_sendmail_with_webversionurl')) > 0 ? $button : "";

        $image = '<img src="' . $this->create_event($insert) . '" alt="image" style="display:none;width:0;height:0;visibility:hidden;">';
        $message["check_open_image"] = intval(c_key('webapp_sendmail_with_check_open_img')) > 0 ? $image : "";

        $insert["message"] = $this->load->view($this->template, $message, true);
        $stringAttach = $attach;
        if (is_array($attach) === true) {
            $stringAttach = implode(",", $attach);
        }
        $insert["attach"] = $stringAttach;
        return $insert;
    }

    public function edit(int $id = 0, array $data = []): bool
    {
        return $this->db->where($this->primary_key, $id)->update($this->table, $data);
    }

    public function edit_multi(array $ids = [], array $data = [])
    {
        if (empty($ids) === false) {
            $this->db->where_in($this->primary_key, $ids)->update($this->table, $data);
        }
    }

    public function del(int $id = 0)
    {
        $this->db->from($this->table)->where($this->primary_key, $id)->limit(1)->delete();
    }

    public function get_all(): array
    {
        $limit = $this->get_total();
        return $this->get_list($limit);
    }

    public function get_total(): int
    {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);

        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array
    {
        $this->db->from($this->table);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }

        $this->db->order_by($this->primary_key, "desc");
        if ($page <= 0) {
            $page = 0;
        }
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_one(): array
    {
        $arr_result = $this->get_list();
        if (empty($arr_result) === false) {
            return current($arr_result);
        }
        return [];
    }

    public function get_one_by_id(int $id = 0): array
    {
        $data[$this->primary_key] = $id;
        $this->sql_where = $data;
        return $this->get_one();
    }

    public function send_action_bulk(int $total_batch = 30): array
    {
        $total_send_ok = 0;
        $total_send_error = 0;
        $this->sql_where = ["is_send" => 0];
        $this->sql_order_by = [$this->primary_key => "asc"];
        $listdb = $this->get_list($total_batch);

        if (empty($listdb) === false) {
            $edit_ids = [];
            foreach ($listdb as $data) {
                $sendstatus = $this->send_action($data);
                if ($sendstatus === true) {
                    $total_send_ok++;
                    $edit_ids[] = $data[$this->primary_key];
                } else {
                    $total_send_error++;
                }
            }
            $dataedit["is_send"] = 1;
            $dataedit["send_date"] = date('Y-m-d H:i:s');
            $this->edit_multi($edit_ids, $dataedit);
        }

        $arr["total_batch"] = $total_batch;
        $arr["total_send_ok"] = $total_send_ok;
        $arr["total_send_error"] = $total_send_error;
        return $arr;
    }
}
