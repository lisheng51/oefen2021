<?php

class Mail_config_model extends MY_Model
{

    public $table = "mail_config";
    public $primary_key = "mail_config_id";
    public $select_order_by = [];
    protected $using_softDel = true;

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'mail_config_id#desc' => 'ID (aflopend)',
            'user#desc' => 'Gebruikersnaam (aflopend)',
            'user#asc' => 'Gebruikersnaam (oplopend)',
            'name#desc' => 'Afzender naam (aflopend)',
            'name#asc' => 'Afzender naam (oplopend)'
        ];
    }

    public function sendMailData(string $from_email = "")
    {
        $result["Host"] = c_key('webapp_smtp_host');
        $result["Username"] = c_key('webapp_smtp_user');
        $result["Password"] = $this->config_model->decryptData(c_key('webapp_smtp_pass'));
        $result["SMTPSecure"] = c_key('webapp_smtp_crypto');
        $result["Port"] = (int) c_key('webapp_smtp_port');
        $result["Name"] = c_key('webapp_smtp_user_name');
        if (empty($from_email) === false) {
            $mailConfig = $this->get_one_by_field('user', $from_email);
            if (empty($mailConfig) === false) {
                $result["Host"] = $mailConfig['host'];
                $result["Username"] = $mailConfig['user'];
                $result["Password"] = $this->mail_config_model->decryptData($mailConfig["pass"]);
                $result["SMTPSecure"] = $mailConfig['crypto'];
                $result["Port"] = (int) $mailConfig['port'];
                $result["Name"] = $mailConfig['name'];
            }
        }
        return $result;
    }

    public function select(string $user = "", string $name = '', bool $with_empty = true, array $dataWhere = []): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if (empty($user) === true) {
            $user = $this->input->post_get($select_name) ?? "";
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $this->sql_where = array_merge($data, $dataWhere);
        $this->sql_order_by = ['name' => 'asc'];
        $select = '<select name=' . $select_name . ' class="form-control selectpicker" data-live-search="true">';
        $emptyOptionText = c_key('webapp_smtp_user_name') . ' (' . c_key('webapp_smtp_user') . ')';
        $select .= $with_empty === true ? '<option value="" >' . $emptyOptionText . '</option>' : '';
        $listdb = $this->get_all();
        foreach ($listdb as $rs) {
            $ckk = $user == $rs['user'] ? "selected" : '';
            $optionText = $rs["name"] . ' (' . $rs["user"] . ')';
            $select .= '<option value=' . $rs['user'] . ' ' . $ckk . '>' . $optionText . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function selectMultiple(array $haystack = [], string $name = '', array $dataWhere = []): string
    {
        $select_name = empty($name) === false ? $name : $this->primary_key;
        if (empty($haystack) === true) {
            $haystack = $this->input->post_get($select_name) ?? [];
            if (is_array($haystack) === false) {
                $haystack = [$haystack];
            }
        }
        $this->sqlReset();
        $data[$this->_field_is_deleted] = 0;
        $this->sql_where = array_merge($data, $dataWhere);
        $this->sql_order_by = ['name' => 'asc'];
        $select = '<select name="' . $select_name . '[]" class="form-control selectpicker" data-selected-text-format="count > 3" data-live-search="true" multiple title="------">';
        $listdb = $this->get_all();
        $emptyOptionText = c_key('webapp_smtp_user_name') . ' (' . c_key('webapp_smtp_user') . ')';
        $select .= '<option value="'.c_key('webapp_smtp_user').'" >' . $emptyOptionText . '</option>';
        foreach ($listdb as $rs) {
            $ckk = (empty($haystack) === false && in_array($rs['user'], $haystack) === true) ? 'selected' : '';
            $optionText = $rs["name"] . ' (' . $rs["user"] . ')';
            $select .= '<option value=' . $rs['user'] . ' ' . $ckk . '>' . $optionText . '</option>';
        }
        $select .= '</select>';
        return $select;
    }

    public function getPostdata(): array
    {
        $data["host"] = $this->input->post("host");
        $data["user"] = $this->input->post("user");
        $data["crypto"] = $this->input->post("crypto");
        $data["port"] = $this->input->post("port");
        $data["name"] = $this->input->post("name");
        $pass = $this->input->post("pass");
        $data["pass"] = $this->encryptData($pass);
        $this->ajaxck_model->ck_value('host', $data["host"]);
        $this->ajaxck_model->ck_value('user', $data["user"]);
        $this->ajaxck_model->ck_value('pass', $data["pass"]);
        $this->ajaxck_model->ck_value('port', $data["port"]);
        $this->ajaxck_model->ck_value('name', $data["name"]);
        return $data;
    }

    public function decryptData(string $result = ""): string
    {
        return $this->encryption->decrypt($result);
    }

    public function encryptData(string $result = ""): string
    {
        return $this->encryption->encrypt($result);
    }
}
