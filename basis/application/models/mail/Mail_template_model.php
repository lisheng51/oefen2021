<?php

class Mail_template_model extends CI_Model
{

    private $folderName = "mail_template";

    public function selectFolder(string $moduleName = ''): array
    {
        $folderPath = DIRECTORY_SEPARATOR . $this->folderName . DIRECTORY_SEPARATOR;
        if (empty($moduleName) === false) {
            $folderPath = 'modules' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . $this->folderName . DIRECTORY_SEPARATOR;
        }
        $modulesmap = directory_map(APPPATH . $folderPath, 1);
        $arrResult = [];
        if (empty($modulesmap) === false) {
            foreach ($modulesmap as $module) {
                $arrResult[] = rtrim($module, DIRECTORY_SEPARATOR);
            }
        }
        return $arrResult;
    }

    private function file(string $langname = "", string $filename = "core")
    {
        if (empty($langname) === true) {
            $langname = $this->language_model->default_lang;
        }
        $file = $filename . '.php';
        return APPPATH . $this->folderName . DIRECTORY_SEPARATOR . $langname . DIRECTORY_SEPARATOR . $file;
    }

    public function fetchfolderFile(string $langname = '', string $filename = "")
    {
        $file_path = $this->file($langname, $filename);
        $lang = [];
        if (file_exists($file_path) === false) {
            return $lang;
        }
        include($file_path);
        return $lang;
    }

    public function fetchfolderFiles(string $language = "")
    {
        if (empty($language) === true) {
            $language = $this->language_model->default_lang;
        }

        $arrResult = [];
        $modulesmap = directory_map(APPPATH . DIRECTORY_SEPARATOR . $this->folderName . DIRECTORY_SEPARATOR . $language . DIRECTORY_SEPARATOR, 1);
        if (empty($modulesmap) === false) {
            foreach ($modulesmap as $module) {
                $extension = get_mime_by_extension($module);
                if ($extension !== false) {
                    $arrResult[] = str_replace('.php', '', $module);
                }
            }
        }
        return $arrResult;
    }

    public function saveFolderFile(array $data = [], string $langname = '', string $filename = "")
    {
        if (empty($data) === true) {
            return false;
        }
        $file_path = $this->file($langname, $filename);
        if (file_exists($file_path) === true) {
            $writefile = "<?php\r\n";
            foreach ($data as $key => $value) {
                $value = nl2br($value);
                $writefile .= '$lang["' . $key . '"] = "' . addslashes($value) . '";' . PHP_EOL;
            }
            return write_file($file_path, $writefile);
        }

        return false;
    }

    public function moduleFetchfolderFiles(string $language = "", string $moduleName = "")
    {
        $arrResult = [];
        if (empty($moduleName) === false && empty($language) === false) {
            $modulesmap = directory_map(APPPATH . 'modules' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . $this->folderName . DIRECTORY_SEPARATOR . $language . DIRECTORY_SEPARATOR, 1);
            if (empty($modulesmap) === false) {
                foreach ($modulesmap as $module) {
                    $extension = get_mime_by_extension($module);
                    if ($extension !== false) {
                        $arrResult[] = str_replace('.php', '', $module);
                    }
                }
            }
        }
        return $arrResult;
    }

    public function moduleFetchfolderFile(string $langname = '', string $moduleName = "", string $filename = "")
    {
        $file_path = $this->moduleFile($langname, $moduleName, $filename);
        $lang = [];
        if (file_exists($file_path) === false) {
            return $lang;
        }
        include($file_path);
        return $lang;
    }

    private function moduleFile(string $langname = "", string $moduleName = "", string $filename = "core")
    {
        if (empty($langname) === true) {
            $langname = $this->language_model->default_lang;
        }

        $file = $filename . '.php';
        $file_path = "";
        if (empty($moduleName) === false && empty($langname) === false) {
            $file_path = APPPATH . 'modules' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . $this->folderName . DIRECTORY_SEPARATOR . $langname . DIRECTORY_SEPARATOR . $file;
        }
        return $file_path;
    }

    public function moduleSaveFolderFile(array $data = [], string $langname = '', string $moduleName = "", string $filename = "")
    {
        if (empty($data) === true) {
            return false;
        }
        $file_path = $this->moduleFile($langname, $moduleName, $filename);
        if (file_exists($file_path) === true) {
            $writefile = "<?php\r\n";
            foreach ($data as $key => $value) {
                $writefile .= '$lang["' . $key . '"] = "' . addslashes($value) . '";' . PHP_EOL;
            }
            return write_file($file_path, $writefile);
        }

        return false;
    }

    public function moduleGetByMethod(string $moduleName = "", string $method = 'contact'): array
    {
        $langname = $this->language_model->get_language();
        $configValue = $this->moduleFetchfolderFile($langname, $moduleName, $method);
        if (empty($configValue) === true) {
            $configValue = $this->moduleFetchfolderFile($this->language_model->default_lang, $moduleName, $method);
        }
        return $configValue;
    }

    public function getByMethod(string $method = 'contact'): array
    {
        $langname = $this->language_model->get_language();
        $configValue = $this->fetchfolderFile($langname, $method);
        if (empty($configValue) === true) {
            $configValue = $this->fetchfolderFile($this->language_model->default_lang, $method);
        }
        return $configValue;
    }

    public function replace_body(string $body = '', array $userdb = []): string
    {
        if (isset($userdb["emailaddress"]) === true) {
            $body = str_replace('{emailaddress}', $userdb["emailaddress"], $body);
        }

        if (isset($userdb["email"]) === true) {
            $body = str_replace('{emailaddress}', $userdb["email"], $body);
        }

        if (isset($userdb["phone"]) === true) {
            $body = str_replace('{phone}', $userdb["phone"], $body);
        }

        if (isset($userdb["name"]) === true) {
            $body = str_replace('{sys_name}', $userdb["name"], $body);
        }

        if (isset($userdb["message"]) === true) {
            $body = str_replace('{sys_message}', $userdb["message"], $body);
        }

        return $body;
    }
}
