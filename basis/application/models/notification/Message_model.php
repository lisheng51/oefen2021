<?php

class Message_model extends MY_Model
{

    public $table = "message";
    public $primary_key = "message_id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'message_id#desc' => 'ID (aflopend)',
            'title#desc' => 'Onderwerp (aflopend)',
            'title#asc' => 'Onderwerp (oplopend)',
            'date#desc' => 'Datum (aflopend)',
            'date#asc' => 'Datum (oplopend)',
        ];
    }

    public function get_postdata(): array
    {
        $data["content"] = $this->input->post("content");
        $data["title"] = $this->input->post("title");
        $data["to_user_id"] = $this->input->post("to_user_id");
        $data["from_user_id"] = $this->login_model->user_id();
        if ($data["from_user_id"] == $data["to_user_id"]) {
            $json["msg"] = "Het heeft geen zin om naar uwzelf te sturen!";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $this->ajaxck_model->ck_value('Content', $data["content"]);
        $this->ajaxck_model->spamword($data);
        return $data;
    }

    public function select_open_status($is_open = "")
    {
        $arr_type = array("no" => "Ongelezen", "yes" => "Gelezen");
        $select = '<select name="is_open" class="form-control selectpicker">';
        $select .= "<option value='' >------</option>";
        foreach ($arr_type as $key => $value) {
            $ckk = $is_open == $key ? "selected" : '';
            $select .= "<option value={$key} $ckk >$value</option>";
        }
        $select .= '</select>';
        return $select;
    }

    public function get_no_open_message(int $uid = 0, int $limit = 10)
    {
        if ($uid > 0) {
            $data_where["to_user_id"] = $uid;
        } else {
            $data_where["to_user_id"] = $this->login_model->user_id();
        }
        $data_where["is_open"] = 0;
        $this->sql_where = $data_where;
        $arr_result = [];
        $total = $this->get_total();
        $listdb = $this->get_list($limit);
        foreach ($listdb as $rs) {
            $rs["from_user_name"] = $this->user_model->fetch_name($rs["from_user_id"]);
            $rs["date"] = date_format(date_create($rs["date"]), 'd-m-Y H:i:s');
            $arr_result[] = $rs;
        }

        $result["listdb"] = $arr_result;
        $result["total"] = $total;
        return $result;
    }
}
