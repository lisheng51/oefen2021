<?php

class History_url_model extends MY_Model
{

    public $table = "history_url";
    public $primary_key = "url_id";
    public $select_order_by = [];

    public function __construct()
    {
        parent::__construct();
        $this->select_order_by = [
            'url_id#desc' => 'ID (aflopend)',
            'date#desc' => 'Datum (aflopend)',
            'date#asc' => 'Datum (oplopend)',
            'path#desc' => 'Path (aflopend)',
            'path#asc' => 'Path (oplopend)',
        ];
    }

    public function update(string $title = "", int $uid = 0)
    {
        if (empty($title) === true) {
            return;
        }
        $data["title"] = $title;
        if ($uid > 0) {
            $data["user_id"] = $uid;
        } else {
            $data["user_id"] = $this->login_model->user_id();
        }
        $data["path"] = uri_string();
        $datack["date >="] = date('Y-m-d 00:00:00');
        $datack["date <="] = date('Y-m-d 23:59:59');
        $this->sql_where = array_merge($data, $datack);
        $exist = $this->get_one();
        if (empty($exist) === true) {
            return $this->db->insert($this->table, $data);
        }
        return $this->db->where($this->primary_key, $exist[$this->primary_key])->update($this->table, ["date" => date("Y-m-d H:i:s")]);
    }

    public function get_last(int $limit = 10)
    {
        $data_where["user_id"] = $this->login_model->user_id();
        $data_where["path !="] = uri_string();
        $this->sql_where = $data_where;
        return $this->get_list($limit);
    }

    public function get_total(): int
    {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);
        $this->db->join($this->user_model->table, $this->user_model->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_or_like) === false) {
            foreach ($this->sql_or_like as $field => $value) {
                $this->db->or_like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array
    {
        $this->db->select($this->table . '.*');
        $this->db->select($this->user_model->table . '.emailaddress');
        $this->db->select($this->user_model->table . '.display_info');
        $this->db->from($this->table);
        $this->db->join($this->user_model->table, $this->user_model->primary_key);
        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_or_like) === false) {
            foreach ($this->sql_or_like as $field => $value) {
                $this->db->or_like($field, $value);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }

        $this->db->order_by('date', "desc");
        if ($page <= 0) {
            $page = 0;
        }
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }
}
