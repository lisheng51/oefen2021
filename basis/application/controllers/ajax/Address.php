<?php

class Address extends Ajax_Controller
{
    public function data()
    {
        $zipcode = $this->input->post('zipcode') ?? '';
        $houseNumber = $this->input->post('housenr') ?? '';

        $json["city"] = null;
        $json["province"] = null;
        $json["street"] = null;
        $json["municipality"] = null;
        $json["country"] = null;
        $json["lat"] = 0;
        $json["lng"] = 0;
        $json["msg"] = "Geen adres gevonden!";
        $json["status"] = "error";

        if (empty($zipcode) === true || empty($houseNumber) === true) {
            exit(json_encode($json));
        }

        $housenr_addition = $this->input->post('housenr_addition') ?? '';

        $arrResult = Api_service::addressSearch($zipcode, $houseNumber, $housenr_addition);

        if (empty($arrResult) === true) {
            exit(json_encode($json));
        }

        $json["city"] = $arrResult["city"];
        $json["street"] = $arrResult["street"];
        $json["province"] = $arrResult["state"];
        $json["municipality"] = $arrResult["locality"];
        $json["lat"] =  $arrResult["lat"];
        $json["lng"] =  $arrResult["lng"];
        $json["country"] = "Nederland";
        $json["status"] = "good";
        $json["msg"] = "Postcode is correct";
        exit(json_encode($json));
    }
}
