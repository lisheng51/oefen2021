<?php

class Errors extends Site_Controller
{

    public function index()
    {
        $this->load->database();
        $this->language_model->set_language();
        $data = $this->getData();
        $this->output->set_status_header(404);
        $this->view_layout("index", $data);
    }

    private function getData()
    {
        $type = $this->session->flashdata('page_error_type');
        switch ($type) {
            case "timeout":
                $data["title"] = lang('error_timeout_title');
                $data["message"] = lang('error_timeout_message');
                break;
            case "is_active":
                $data["title"] = lang('error_is_active_title');
                $data["message"] = lang('error_is_active_message');
                break;
            case "no_authorized":
                $data["title"] = lang('error_no_authorized_title');
                $data["message"] = lang('error_no_authorized_message');
                break;
            case "user_no_find":
                $data["title"] = lang('error_user_no_find_title');
                $data["message"] = lang('error_user_no_find_message');
                break;
            case "license_out":
                $data["title"] = lang('error_license_out_title');
                $data["message"] = lang('error_license_out_message');
                break;
            case "custom_infomation":
                $data["title"] = $this->session->flashdata('page_error_title');
                $data["message"] = $this->session->flashdata('page_error_message');
                break;
            default:
                $data["title"] = lang('error_default_title');
                $data["message"] = lang('error_default_message');
                break;
        }

        return $data;
    }
}
