<?php

class Mail extends Site_Controller {

    public function index() {
        redirect(site_url());
    }

    public function sys() {
        $this->load->database();
        $hash = rawurldecode($this->input->get('hashkey'));
        $arr_keys = explode("#_#", $this->encryption->decrypt($hash));

        $data["event_value"] = $arr_keys[0];
        $data["to_email"] = $arr_keys[1];
        $this->sendmail_model->sql_where = $data;
        $arr_db = $this->sendmail_model->get_one();

        if (empty($arr_db) === true) {
            $this->index();
        }

        exit($arr_db["message"]);
    }

    public function sys_open() {
        $this->load->database();
        $hash = rawurldecode($this->input->get('hashkey'));
        $arr_keys = explode("#_#", $this->encryption->decrypt($hash));
        $data["event_value"] = $arr_keys[0];
        $data["to_email"] = $arr_keys[1];
        $data["is_open"] = 0;
        $this->sendmail_model->sql_where = $data;
        $arr_db = $this->sendmail_model->get_one();
        if (empty($arr_db) === false) {
            $edit["is_open"] = 1;
            $edit["open_date"] = date('Y-m-d H:i:s');
            $this->sendmail_model->edit($arr_db[$this->sendmail_model->primary_key], $edit);
        }
        $im = imagecreatetruecolor(100, 100);
        imagefilledrectangle($im, 0, 0, 99, 99, 0xFFFFFF);
        imagegif($im);
        imagedestroy($im);
        $this->output->set_content_type('image/gif')->set_output($im);
    }

}
