<?php

class Whitelist extends API_Controller
{

    private function whitelistModule(string $module = "")
    {
        $module = rtrim($module, DIRECTORY_SEPARATOR);
        $array = directory_map(APPPATH . 'modules/' . $module . '/controllers');
        $array_sys_model = [];
        foreach ($array as $folder => $value) {
            $folder = rtrim($folder, DIRECTORY_SEPARATOR);
            if (is_array($value) === true) {
                foreach ($value as $path) {
                    $array_sys_model[] = strtolower($module . '/' . $folder . '/' . rtrim($path, ".php"));
                }
            } else {
                $array_sys_model[] = strtolower(rtrim($module . '/' . $value, ".php"));
            }
        }
        return $array_sys_model;
    }

    private function whitelistSys()
    {
        $array = directory_map(APPPATH . 'controllers');
        $array_sys_model = [$this->upload_model->root_folder, 'assets'];
        foreach ($array as $folder => $value) {
            $folder = rtrim($folder, DIRECTORY_SEPARATOR);
            if (is_array($value) === true) {
                foreach ($value as $path) {
                    if (is_array($path) === true) {
                        continue;
                    }
                    $array_sys_model[] = strtolower($folder . '/' . rtrim($path, ".php"));
                }
            } else {
                $array_sys_model[] = strtolower(rtrim($value, ".php"));
            }
        }
        return $array_sys_model;
    }

    private function routes()
    {
        $arrayRoutes = [];
        $array = array_keys($this->router->routes);
        if (empty($array) === false) {
            foreach ($array as $value) {
                if ($value === '404_override') {
                    continue;
                }
                $arrayRoutes[] = $value;
            }
        }

        return $arrayRoutes;
    }

    public function index()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $listdb[] = $this->routes();
        $listdb[] = $this->whitelistSys();
        $array_sub = directory_map(APPPATH . 'modules');
        if (empty($array_sub) === false) {
            $modules = array_keys($array_sub);
            foreach ($modules as $module) {
                $arr = $this->whitelistModule($module);
                if (empty($arr) === false) {
                    $listdb[] = $arr;
                }
            }
        }
        $whitePaths = array_reduce($listdb, 'array_merge', []);
        exit(json_encode($whitePaths));
    }

    public function login()
    {
        $jsondata = $newData = [];
        $this->user_model->sql_where = ["date!=" => null];
        $listdb = $this->user_model->get_all();
        foreach ($listdb as $rs) {
            $newData["datetime"] = $rs["date"];
            $newData["ip"] = $rs["ip_address"];
            $jsondata[] = $newData;
        }
        exit(json_encode($jsondata));
    }
}
