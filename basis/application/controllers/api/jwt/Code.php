<?php

class Code extends API_Controller
{

    public function access()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $tokenString = $this->encryption->encrypt($this->apiId);
        $token = rawurlencode($tokenString);
        $this->api_model->outOK($token, $apiLogId);
    }

    public function index()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $postData = empty($this->arrPost) === true ? $this->input->post() : $this->arrPost;

        $emailaddress = $postData["username"] ?? null;
        if ($emailaddress === null || empty($emailaddress) === true) {
            $this->api_model->out('Geen email gevonden', 98);
        }

        $data["access_code"] = F_string::random(6);
        $data["datetime"] = date('Y-m-d H:i:s', strtotime('+30 minutes'));

        $dataMessage["content"] = 'Beste gebruiker, uw toegangscode is ' . $data["access_code"] . ', het is geldig tot ' . $data["datetime"];
        $dataMessage["title"] = 'Uw toegangscode';
        $this->sendmail_model->add_send($dataMessage, $emailaddress);

        $valuestring = json_encode($data);
        $encrypted_string = $this->encryption->encrypt($valuestring);
        $hashkey = rawurlencode($encrypted_string);

        $json["hashkey"] = $hashkey;
        $json["msg"] = "Er is een mail gestuurd";
        $json["status"] = "good";
        $this->api_model->out($json, 100, $apiLogId);
    }

    public function access_datetime()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $postData = empty($this->arrPost) === true ? $this->input->post() : $this->arrPost;
        $sec = $postData["sec"] ?? 60;
        $json[$this->api_model->primary_key] = $this->apiId;
        $json["datetime"] = date('Y-m-d H:i:s', time() + $sec);
        $valuestring = json_encode($json);
        $encrypted_string = $this->encryption->encrypt($valuestring);
        $hashkey = rawurlencode($encrypted_string);
        $this->api_model->outOK($hashkey, $apiLogId);
    }

    public function accessCode()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $postData = empty($this->arrPost) === true ? $this->input->post() : $this->arrPost;

        $username = $postData["username"] ?? null;
        if ($username === null || empty($username) === true) {
            $this->api_model->out('Geen gebruikersnaam gevonden', 98);
        }

        $user_db = $this->login_model->get_one_by_username_email($username);
        if (empty($user_db) === true || $user_db["is_del"] == 1) {
            $this->api_model->out('Er is geen gebruiker gevonden', 94);
        }

        $sendStatus = $this->sendmail_model->access_code_login($user_db);
        if ($sendStatus === false) {
            $json["msg"] = "Even druk, mail wordt wat later verzonden";
            $json["status"] = "good";
            $this->api_model->out($json, 100, $apiLogId);
        }
        $json["msg"] = "Er is een mail gestuurd";
        $json["status"] = "good";
        $this->api_model->out($json, 100, $apiLogId);
    }


    public function checkAccessCode()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $postData = empty($this->arrPost) === true ? $this->input->post() : $this->arrPost;

        $username = $postData["username"] ?? null;
        if ($username === null || empty($username) === true) {
            $this->api_model->out('Geen gebruikersnaam gevonden', 98);
        }

        $access_code = $postData["access_code"] ?? null;
        if ($access_code === null || empty($access_code) === true) {
            $this->api_model->out('Geen code gevonden', 98);
        }

        $arr_rs = $this->login_model->get_one_by_username_email($username);

        if (empty($arr_rs) === true || empty($arr_rs["access_code"]) === true || $arr_rs["is_del"] == 1) {
            $this->api_model->out('Er is geen gebruiker gevonden', 94);
        }

        if (date("Y-m-d H:i:s") > $arr_rs["access_code_date"]) {
            $this->api_model->out('Code is verlopen', 95);
        }

        if ($access_code !== $arr_rs["access_code"]) {
            $this->api_model->out('Code is niet juist', 97);
        }

        $json["msg"] = "Code is juist";
        $json["status"] = "good";
        $this->api_model->out($json, 100, $apiLogId);
    }
}
