<?php

class Language extends API_Controller
{

    public function list()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $arrResult = $this->language_model->selectList();
        $this->api_model->outOK($arrResult, $apiLogId);
    }

    public function file()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $language = $this->input->post('language');
        $moduleName = $this->input->post('module');
        $arrResult = [];

        if (empty($moduleName) === false && empty($language) === false) {
            $modulesmap = directory_map(APPPATH . 'modules' . DIRECTORY_SEPARATOR . $moduleName . DIRECTORY_SEPARATOR . 'language' . DIRECTORY_SEPARATOR . $language . DIRECTORY_SEPARATOR, 1);
            foreach ($modulesmap as $module) {
                $extension = get_mime_by_extension($module);
                if ($extension !== false) {
                    $arrResult[] = str_replace('_lang.php', '', $module);
                }
            }
            $this->api_model->outOK($arrResult, $apiLogId);
        }

        if (empty($language) === false) {
            $modulesmap = directory_map(APPPATH . DIRECTORY_SEPARATOR . 'language' . DIRECTORY_SEPARATOR . $language . DIRECTORY_SEPARATOR, 1);
            foreach ($modulesmap as $module) {
                $arrResult[] = str_replace('_lang.php', '', $module);
            }
            $this->api_model->outOK($arrResult, $apiLogId);
        }

        $this->api_model->outNOK(99, "Geen bestand gevonden", $apiLogId);
    }

    public function index()
    {
        $apiLogId = $this->addLog(__METHOD__);
        $language = $this->input->post('language');
        $moduleName = $this->input->post('module');
        $filename = $this->input->post('file');
        $lang = [];
        if (empty($language) === false) {
            $data_file = APPPATH . "language/$language/" . $filename . '_lang.php';
            if (file_exists($data_file) === true) {
                require($data_file);
            }

            if (empty($moduleName) === false) {
                $ck_file_default = APPPATH . "modules/$moduleName/language/$language/" . $filename . '_lang.php';
                if (file_exists($ck_file_default) === true) {
                    require($ck_file_default);
                }
            }

            $this->api_model->outOK($lang, $apiLogId);
        }

        $this->api_model->outNOK(99, "Geen bestand gevonden", $apiLogId);
    }
}
