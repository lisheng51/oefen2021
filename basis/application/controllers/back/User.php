<?php

class User extends Back_Controller
{

    protected static $title_module = 'Account';

    public function switchUser(int $userId = 0)
    {
        if ($this->login_model->user_id() !== $this->user_model->secure_id) {
            redirect($this->controller_url);
        }

        $arr_user = $this->user_model->get_one_by_id(intval($userId));
        if (empty($arr_user) === true) {
            redirect($this->controller_url);
        }

        if ($arr_user["is_active"] == 0 || $arr_user["is_del"] == 1) {
            redirect($this->controller_url);
        }

        $this->login_model->login_user_data = $arr_user;
        $this->login_model->add_user_session();
        redirect($this->access_check_model->redirect_url());
    }

    public function updateBypermissionGroup()
    {
        $id = $this->input->post($this->user_model->primary_key) ?? 0;
        if ($id <= 0) {
            $json["msg"] = self::$title_module . ' is niet gevonden!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $status = $this->bookmark_model->resetAllByPermission($id);
        if ($status === true) {
            $json["msg"] = 'Bookmark als navbar is bijgewerkt!';
            $json["status"] = "good";
            exit(json_encode($json));
        }
    }

    public function profile()
    {
        if ($this->login_model->user_id() === $this->user_model->secure_id) {
            redirect($this->controller_url . "/edit/" . $this->arr_userdb[$this->user_model->primary_key]);
        }
        if ($this->input->post()) {
            $this->profileAction();
        }
        $rsdb = $this->arr_userdb;
        $data["title"] = self::$title_module . ' wijzigen';
        $data['rsdb'] = $rsdb;
        $data["edit_email_url"] = site_url($this->controller_url . "/edit_email");
        $data["select_2fa_status"] = select_boolean('with_2fa', intval($rsdb["with_2fa"] ?? 0));
        $data['breadcrumb'] = "";
        $this->view_layout("profile", $data);
    }

    public function index()
    {
        //$this->global_model->redirectWithPageNumber();
        $data_where[] = [$this->user_model->table . '.' . $this->user_model->_field_is_deleted => 0];
        $data_where[] = [$this->user_model->table . '.' . $this->user_model->primary_key . ' NOT IN (' . $this->user_model->secure_id . ')' => 'value_is_null'];
        // $data_where[] = setFieldAndOperator($this->permission_group_model->primary_key, $this->user_model->table . '.permission_group_ids',  loadPostGet($this->permission_group_model->primary_key, 'array'), false, loadPostGet($this->permission_group_model->primary_key . '_operator'));
        // $data_where[] = setFieldAndOperator('search_email', $this->user_model->table . '.emailaddress', loadPostGet('search_email'), false, loadPostGet('search_email_operator'));
        // $data_where[] = setFieldAndOperator('search_name', $this->user_model->table . '.name', loadPostGet('search_name'));
        // $is_active = $this->input->post_get("is_active") ?? loadPostGet('is_active') ?? "";

        $data_where[] = setFieldAndOperator('search_email', $this->user_model->table . '.emailaddress');
        $data_where[] = setFieldAndOperator($this->permission_group_model->primary_key, $this->user_model->table . '.permission_group_ids');
        $data_where[] = setFieldAndOperator('search_name', $this->user_model->table . '.name');

        $is_active = $this->input->post_get("is_active") ?? "";
        if ($is_active != "") {
            $is_active = intval($is_active);
            $data_where[] = [$this->user_model->table . '.' . "is_active" => $is_active];
        }

        $this->user_model->setSqlWhere($data_where);
        $this->user_model->sql_order_by = setFieldOrderBy();

        $total = $this->user_model->get_total();
        $data["listdb"] = $this->getList();
        $data["total"] = $total;
        $data["pagination"] = $this->global_model->show_page($total);
        $data["result"] = $this->view_layout_return("ajax_list", $data);

        if ($this->input->post()) {
            $this->global_model->savePostGet();
            $json["result"] = $data["result"];
            exit(json_encode($json));
        }
        $data["title"] = self::$title_module . ' overzicht';
        $data["addButton"] = addButton($this->controller_name . '.add', $this->controller_url . '/add');
        $this->view_layout("index", $data);
    }

    private function getList()
    {
        $page_limit = $this->input->post("page_limit");
        $limit = empty($page_limit) === true ? c_key('webapp_default_show_per_page') : $page_limit;

        $page_number = $this->input->get("page_number");
        $page = empty($page_number) === true ? 0 : ($page_number * $limit) - $limit;

        $arr_result = [];
        $listdb = $this->user_model->get_list($limit, $page);
        foreach ($listdb as $rs) {
            $rs['permissionGroupDb'] = $this->permission_group_model->get_all_group($rs['permission_group_ids'] ?? "");
            $rs["editButton"] = editButton($this->controller_name . '.edit', $this->controller_url . '/edit/' . $rs[$this->user_model->primary_key]);
            $arr_result[] = $rs;
        }

        return $arr_result;
    }

    public function del()
    {
        $id = $this->input->post("del_id");
        $rsdb = $this->user_model->get_one_by_id(intval($id));
        if (empty($rsdb) === true || $id === $this->user_model->secure_id || $id == $this->arr_userdb["user_id"]) {
            $json["msg"] = self::$title_module . ' kan niet worden verwijderd!';
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $data["is_del"] = 1;
        $data["is_active"] = 0;
        $this->user_model->edit($id, $data);

        $dataLogin["ip_address"] = null;
        $dataLogin["browser"] = null;
        $dataLogin["platform"] = null;
        $dataLogin["password_date"] = null;
        $dataLogin["is_online"] = 0;
        $dataLogin["with_2fa"] = 0;
        $dataLogin["date"] = null;
        $dataLogin["access_code"] = null;
        $dataLogin["access_code_date"] = null;
        $this->login_model->edit($id, $dataLogin);

        $json["msg"] = self::$title_module . ' is verwijderd!';
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url);
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function add()
    {
        if ($this->input->post()) {
            $this->addAction();
        }
        $data["title"] = self::$title_module . ' toevoegen';
        $data['rsdb'] = null;
        $data["logindata_show"] = "d-none";
        $data["delButton"] = null;
        $data["select_multiple_permissionGroup"] = $this->permission_group_model->selectMultiple();
        $data["select_2fa_status"] = null;
        $this->view_layout("edit", $data);
    }

    private function addAction()
    {
        $data = $this->get_postdata();
        $check_email = $this->login_model->get_one_by_username_email($data["emailaddress"]);
        if (empty($check_email) === false && $check_email["is_del"] > 0) {
            $data_reset["is_del"] = 0;
            $this->user_model->edit($check_email["user_id"], $data_reset);
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is toegevoegd!';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            //$this->sendmail_model->user_active($check_email);
            exit(json_encode($json));
        }

        if (empty($check_email) === false) {
            $json["msg"] = "Het emailadres bestaat al!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $uid = $this->user_model->add($data);
        if ($uid > 0) {
            $data_login["user_id"] = $uid;
            $data_login["username"] = $data["emailaddress"];
            $data_login["redirect_url"] = $this->input->post("redirect_url");
            $data_login["password_date"] = date('Y-m-d H:i:s');
            $password = $this->input->post("password");
            $data_login["password"] = password_hash($password, PASSWORD_DEFAULT);
            if ($this->input->post("with_access_code") == "on") {
                $data_login["with_access_code"] = 1;
            }
            $this->login_model->add($data_login);
            $this->upload_type_model->make_dir($uid);
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            $json["msg"] = self::$title_module . ' is toegevoegd!';
            $json["status"] = "good";
            add_app_log($json["msg"]);
            //$this->sendmail_model->user_active($this->user_model->get_one_by_id($uid));
            exit(json_encode($json));
        }
    }

    private function get_postdata()
    {
        if ($this->input->post("del_id")) {
            $this->del();
        }
        $data = $this->user_model->get_postdata();
        $data["emailaddress"] = strtolower($this->input->post("emailaddress"));
        $data["is_active"] = $this->input->post("is_active");
        $this->ajaxck_model->ck_value('email', $data["emailaddress"]);

        $permission_group_ids = $this->input->post("permission_group_id");
        if (empty($permission_group_ids) === true) {
            $json["msg"] = 'Toestemming groep is leeg!';
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $data["permission_group_ids"] = implode(',', $permission_group_ids);
        return $data;
    }

    public function edit($id = 0)
    {
        $this->user_model->secure_id(intval($id));
        if ($this->input->post()) {
            $this->editAction();
        }

        $rsdb = $this->user_model->get_one_by_id(intval($id));
        if (empty($rsdb) === true) {
            redirect($this->controller_url);
        }

        $data["logindata_show"] = null;
        $data['rsdb'] = $rsdb;
        $data["delButton"] = delButton($this->controller_name . '.del', $id);
        $data["select_multiple_permissionGroup"] = $this->permission_group_model->selectMultiple(explode(',', $rsdb["permission_group_ids"]));
        $data["select_2fa_status"] = select_boolean('with_2fa', intval($rsdb["with_2fa"] ?? 0));
        $data["title"] = self::$title_module . ' wijzigen';
        $this->view_layout("edit", $data);
    }

    private function editAction()
    {
        $uid = $this->input->post("user_id");
        $password = $this->input->post("password");
        $data = $this->get_postdata();

        $check_email = $this->login_model->get_one_by_username_email($data["emailaddress"]);
        $rsdb = $this->user_model->get_one_by_id($uid);
        if (($data["emailaddress"] !== $rsdb["emailaddress"]) && empty($check_email) === false) {
            $json["msg"] = "Het emailadres bestaat al!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $data_login["username"] = $data["emailaddress"];
        if (empty($password) === false) {
            if ($this->input->post("with_access_code") == "on") {
                $data_login["with_access_code"] = 1;
            }
            if (password_verify($password, $rsdb["password"]) === true) {
                $json["msg"] = "Wachtwoord mag niet hetzelfde zijn als de vorige!";
                $json["status"] = "error";
                exit(json_encode($json));
            }
            $data_login["password_date"] = date('Y-m-d H:i:s');
            $data_login["password"] = password_hash($password, PASSWORD_DEFAULT);
        }

        $data_login["redirect_url"] = $this->input->post("redirect_url");
        $data_login["with_2fa"] = 0;

        if ($rsdb["with_2fa"] == 0 && $this->input->post("with_2fa") > 0 && empty($this->input->post("webapp_one_code")) === true) {
            $json["msg"] = "2fa code is leeg!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $resultStatus2fa = $this->login_model->check2FAStatus($uid, $this->input->post("webapp_one_code"));
        if ($rsdb["with_2fa"] == 0 && $this->input->post("with_2fa") > 0 && $resultStatus2fa === false) {
            $json["msg"] = "2fa code is niet juist!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if ($rsdb["with_2fa"] == 0 && $this->input->post("with_2fa") > 0 && $resultStatus2fa === true) {
            $data_login["with_2fa"] = 1;
            $data_login["2fa_secret"] = $this->session->userdata('2fa_secret');
        }

        if ($rsdb["with_2fa"] > 0 && $this->input->post("with_2fa") > 0) {
            $data_login["with_2fa"] = 1;
        }

        $this->login_model->edit($uid, $data_login);
        if (empty($rsdb) === false) {
            $this->user_model->edit($uid, $data);
            $json["msg"] = self::$title_module . ' is bijgewerkt!';
            $json["status"] = "good";
            $json["type_done"] = "redirect";
            $json["redirect_url"] = site_url($this->controller_url);
            add_app_log($json["msg"]);
            exit(json_encode($json));
        }
    }

    private function profileAction()
    {
        $rsdb = $this->arr_userdb;
        $data = $this->user_model->get_postdata();
        $password = $this->input->post("password");
        $uid = $rsdb["user_id"];
        if (empty($password) === false) {
            $old_password = $this->input->post("old_password");
            if (empty($rsdb["password"]) === false && password_verify($old_password, $rsdb["password"]) === false) {
                $json["msg"] = "Uw oud wachtwoord is niet corret!";
                $json["status"] = "error";
                exit(json_encode($json));
            }
            $this->ajaxck_model->password($password);
            $data_login["password"] = password_hash($password, PASSWORD_DEFAULT);
        }

        $data_login["with_2fa"] = 0;
        $data_login["redirect_url"] = $this->input->post("redirect_url");
        if ($rsdb["with_2fa"] == 0 && $this->input->post("with_2fa") > 0 && empty($this->input->post("webapp_one_code")) === true) {
            $json["msg"] = "2fa code is leeg!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $resultStatus2fa = $this->login_model->check2FAStatus($uid, $this->input->post("webapp_one_code"));
        if ($rsdb["with_2fa"] == 0 && $this->input->post("with_2fa") > 0 && $resultStatus2fa === false) {
            $json["msg"] = "2fa code is niet juist!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if ($rsdb["with_2fa"] == 0 && $this->input->post("with_2fa") > 0 && $resultStatus2fa === true) {
            $data_login["with_2fa"] = 1;
            $data_login["2fa_secret"] = $this->session->userdata('2fa_secret');
        }

        if ($rsdb["with_2fa"] > 0 && $this->input->post("with_2fa") > 0) {
            $data_login["with_2fa"] = 1;
        }

        $this->login_model->edit($uid, $data_login);
        $this->user_model->edit($uid, $data);
        $json["msg"] = "Profiel is bijgewerkt! ";
        $json["status"] = "good";
        $json["type_done"] = "redirect";
        $json["redirect_url"] = site_url($this->controller_url . '/profile');
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    public function edit_email()
    {
        if ($this->input->post()) {
            $this->edit_email_action();
        }

        $data["title"] = "Email wijzigen";
        $data["rsdb"] = $this->arr_userdb;
        $data['breadcrumb'] = "";
        $this->view_layout("edit_email", $data);
    }

    private function edit_email_action()
    {
        $edit_email_code = $this->input->post("edit_email_code");
        $data["emailaddress"] = $this->input->post("emailaddress");
        $password = $this->input->post("password");
        $rsdb = $this->arr_userdb;
        if (empty($rsdb["password"]) === true) {
            $json["msg"] = "Uw moet eerste het wachtwoord instellen";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $this->ajaxck_model->ck_value('wachtwoord', $password);
        $this->ajaxck_model->ck_value('email', $data["emailaddress"]);
        if (password_verify($password, $rsdb["password"]) === FALSE) {
            $json["msg"] = "Uw wachtwoord is niet corret!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        $check_email = $this->login_model->get_one_by_username_email($data["emailaddress"]);
        if (empty($check_email) === false) {
            $json["msg"] = "Het emailadres bestaat al!";
            $json["status"] = "error";
            exit(json_encode($json));
        }

        if (empty($edit_email_code) === true) {
            $this->send_edit_email_code($rsdb, $data["emailaddress"]);
        }

        $code = $this->session->userdata('edit_email_code');
        if ($code !== $edit_email_code) {
            $json["msg"] = "Code is niet corret! ";
            $json["status"] = "error";
            exit(json_encode($json));
        }
        $this->user_model->edit($rsdb["user_id"], $data);
        $data_login["username"] = $data["emailaddress"];
        $this->login_model->edit($rsdb["user_id"], $data_login);
        $this->session->unset_userdata('edit_email_code');
        $json["msg"] = "Email is bijgewerkt! ";
        $json["status"] = "good";
        $json["type_done"] = "change_label";
        $json["input_html"] = '<span class="label label-success">Email is bijgewerkt!</span>';
        add_app_log($json["msg"]);
        exit(json_encode($json));
    }

    private function send_edit_email_code($arr_user = [], $email = "")
    {
        if (empty($arr_user) === true || empty($email) === true) {
            return;
        }

        $this->sendmail_model->edit_email_code($arr_user, $email);

        $json["msg"] = "Uw email validatiecode is naar uw mailbox gestuurd, Voert uw code in:";
        $json["status"] = "good";
        $json["type_done"] = "show_form_input";  //
        $json["input_html"] = ' <div class="input-group-prepend"><span class="input-group-text">Email validatiecode:</span></div><input type="text" class="form-control" name="edit_email_code" required>';  //style="text-transform: uppercase" 
        exit(json_encode($json));
    }
}
