<form enctype="multipart/form-data" method="POST" id="send_multipart">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">Algemeen</div>
                <div class="card-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Type*</label>
                            <?php echo $select_type ?>
                        </div>
                    </div>

                    <div class="col-md-8 <?php echo $title_status ?>">
                        <div class="form-group">
                            <label>Title*</label>
                            <input type="text" class="form-control" placeholder="Naam" name="title" value="<?php echo $rsdb["title"] ?? ""; ?>" />
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Bestand</label>
                            <?php echo $input_file ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <input type="hidden" name="upload_id" value="<?php echo $rsdb["upload_id"] ?? 0; ?>" />
                        <?php echo add_csrf_value(); ?>
                        <?php echo add_submit_button($rsdb) ?>
                        <?php echo add_reset_button() ?>
                        <?php echo $delButton ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<script>
    $("form#send_multipart").submit(function(e) {
        ajax_form_search($(this));
        e.preventDefault();
    });
</script>