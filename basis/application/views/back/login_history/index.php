<script>
    $(function () {
        $("form#form_search").submit(function (e) {
            ajax_form_search($(this));
            e.preventDefault();
        });
        input_reportrange('input[name="reportrange"]');
    });
</script>


<div class="card">
    <div class="card-header"><?php echo lang('search_box_header_text') ?></div>
    <div class="card-body">
        <form method="POST" id="form_search">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Datum</label>
                        <input type="text" name="reportrange" class="form-control" />
                    </div> 
                </div>
                <div class="col-md-3">
                    <?php echo labelSelectInput('Email', 'emailaddress') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php echo add_csrf_value(); ?>
                    <?php echo search_button() ?>
                    <?php echo reset_button() ?>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-md-2">
                <?php echo select_order_by($this->login_history_model->select_order_by); ?>
            </div>
            <div class="col-md-2">
                <?php echo select_page_limit() ?>
            </div>
        </div>
    </div>
</div>


<div class="row mt-3">
    <div class="col-md-12" id="ajax_search_content">
        <?php echo $result; ?>
    </div>
</div>