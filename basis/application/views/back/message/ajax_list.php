<div class="card">
    <div class="card-header">Resultaten - Totaal gevonden: <span class="totalcount"><?php echo $total ?></span></div>
    <div class="card-body">
        <div class="table-responsive">  
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Datum & Tijd</th>
                        <th>Van -> Naar</th>
                        <th>Onderwerp</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="itemContainer">
                    <?php foreach ($listdb as $value) : ?>
                        <tr id="<?php echo $value["message_id"]; ?>">  
                            <td><?php echo $value["date"] ?></td>
                            <td><?php echo $value["from_user_name"] ?> -> <?php echo $value["to_user_name"] ?></td>
                            <td><?php echo $value["title"] ?></td>
                            <td><?php echo $value["viewButton"] ?></td> 
                        </tr>
                    <?php endforeach; ?>   
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer">
        <?php echo $pagination; ?>
    </div>
</div>