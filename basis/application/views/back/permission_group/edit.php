<div class="card">
    <div class="card-header"><?php echo $title ?></div>
    <div class="card-body">
        <form method="POST" id="send">
            <div class="row">
                <div class="col-8">
                    <label>Naam*</label>
                    <div class="form-group">
                        <input type="text" class="form-control" maxlength="50" name="name" required value="<?php echo $rsdb["name"] ?? ""; ?>" />
                    </div>
                </div>
                <div class="col-4">
                    <label>Type*</label>
                    <div class="form-group">
                        <?php echo $this->permission_group_type_model->select($rsdb[$this->permission_group_type_model->primary_key] ?? 1); ?>
                    </div>
                </div>
                <div class="col-12">
                    <ul class="nav nav-tabs">
                        <?php foreach ($permissions as $module => $listdb) : list($name, $toggleId) = explode("#_#", $module); ?>
                            <li class="nav-item"><a class="nav-link" href="#module_id_<?php echo $toggleId ?>" data-toggle="tab"><?php echo $name ?></a></li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="tab-content">
                        <?php foreach ($permissions as $module => $listdb) : list($name, $toggleId) = explode("#_#", $module); ?>
                            <div id="module_id_<?php echo $toggleId ?>" class="tab-pane mt-3">
                                <?php foreach ($listdb as $value) : ?>
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input class="custom-control-input label_module_id_<?php echo $toggleId ?>" type="checkbox" <?php echo $value['checkbox_value'] ?> name="permission_id[]" value="<?php echo $value['permission_id'] ?>" id="label_permission_id_<?php echo $value['permission_id'] ?>">
                                        <label class="custom-control-label" for="label_permission_id_<?php echo $value['permission_id'] ?>"><?php echo $value['checkbox_label'] ?></label>
                                    </div>
                                <?php endforeach; ?>

                                <div class="custom-control custom-checkbox mb-3">
                                    <input class="custom-control-input" type="checkbox" id="all_label_module_id_<?php echo $toggleId ?>">
                                    <label class="custom-control-label" for="all_label_module_id_<?php echo $toggleId ?>"><strong>Alles (de)selecteren</strong></label>
                                </div>
                            </div>

                            <script>
                                $('#all_label_module_id_<?php echo $toggleId ?>').click(function() {
                                    if (this.checked) {
                                        $('.label_module_id_<?php echo $toggleId ?>').each(function() {
                                            this.checked = true;
                                        });
                                    } else {
                                        $('.label_module_id_<?php echo $toggleId ?>').each(function() {
                                            this.checked = false;
                                        });
                                    }
                                });
                            </script>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <input type="hidden" name="<?php echo $this->permission_group_model->primary_key ?>" value="<?php echo $rsdb[$this->permission_group_model->primary_key] ?? 0; ?>" />
                        <?php echo add_csrf_value(); ?>
                        <?php echo add_submit_button($rsdb) ?>
                        <?php echo add_reset_button() ?>
                        <?php echo $delButton; ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    $('.nav a:first').tab('show');
    $("form#send").submit(function(e) {
        ajax_form_search($(this));
        e.preventDefault();
    });
</script>