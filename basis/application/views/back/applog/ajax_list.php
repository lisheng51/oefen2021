<div class="card">
    <div class="card-header">Resultaten - Totaal gevonden: <span class="totalcount"><?php echo $total ?></span></div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" type="checkbox" id="chat_id_selecctall">
                                <label class="custom-control-label font-weight-normal" for="chat_id_selecctall"><strong>Datum & Tijd</strong></label>
                            </div>
                        </th>
                        <th>Gebruikersnaam</th>
                        <th>Beschrijving</th>
                        <th>Path/url</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="itemContainer">
                    <?php foreach ($listdb as $value) : ?>
                        <tr id="<?php echo $value["log_id"]; ?>">
                            <td>
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input chat_id_select" type="checkbox" id="log_id<?php echo $value['log_id'] ?>" name='log_id[]' class="" value="<?php echo $value['log_id'] ?>">
                                    <label class="custom-control-label font-weight-normal" for="log_id<?php echo $value['log_id'] ?>"><?php echo $value["date"] ?></label>
                                </div>
                            </td>
                            <td><?php echo $value["emailaddress"] ?></td>
                            <td><?php echo $value["description"] ?></td>
                            <td><?php echo $value["path"] ?></td>
                            <td>
                                <button type="button" class="btn btn-danger btn-sm delButton" data-search_data="<?php echo $value["log_id"]; ?>" data-del_link="<?php echo $value["del_url"] ?>"><?php echo lang("del_icon") ?></button>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <button type="button" class="btn btn-danger remove_chat_more">Verwijderen</button>
            </div>
        </div>
    </div>

    <div class="card-footer">
        <div class="row">
            <div class="col-12">
                <?php echo $pagination; ?>
            </div>
        </div>
    </div>
</div>

<script>
    $('#chat_id_selecctall').click(function() {
        if (this.checked) {
            $('.chat_id_select').each(function() {
                this.checked = true;
            });
        } else {
            $('.chat_id_select').each(function() {
                this.checked = false;
            });
        }
    });

    $("button.remove_chat_more").click(function(e) {
        if (!$('.chat_id_select').is(':checked')) {
            handle_info_box("error", "Geen log gevonden");
        } else {
            Swal.fire({
                title: 'Bevestig uw keuze',
                icon: 'question',
                text: "Weet u zeker dat u deze wilt verwijderen?",
                showCancelButton: true,
                cancelButtonText: "Nee",
                confirmButtonText: "Ja"
            }).then((result) => {
                if (result.value) {
                    var searchIDs = $(".chat_id_select:checkbox:checked").map(function() {
                        return $(this).val();
                    }).get();
                    var ajaxurl = "<?php echo $ajax_batch_del_url ?>";
                    var senddata = {
                        'log_id': searchIDs
                    };
                    axios_search(ajaxurl, senddata, $("button.remove_chat_more"));
                }
            });
        }
    });
</script>