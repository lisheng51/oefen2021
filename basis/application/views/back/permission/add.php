<div class="card">
    <div class="card-header">Algemeen</div>
    <div class="card-body">
        <form method="POST" id="send">
            <div class="row">
                <div class="col-2">
                    <label>Module*</label>
                    <div class="form-group">
                        <?php echo $this->module_model->select() ?>
                    </div>
                </div>

                <div class="col-2">
                    <label>Link dir*</label>
                    <div class="form-group">
                        <?php echo $this->permission_group_type_model->selectPath(); ?>
                    </div>
                </div>
                <div class="col-2">
                    <label>Object*</label>
                    <div class="form-group">
                        <input type="text" class="form-control" maxlength="50" name="object" required />
                    </div>
                </div>
                <div class="col-2">
                    <label>Method*</label>
                    <div class="form-group">
                        <input type="text" class="form-control" required maxlength="50" name="method" />
                    </div>
                </div>

                <div class="col-2">
                    <label>Is link</label>
                    <div class="form-group">
                        <?php echo select_boolean('has_link'); ?>
                    </div>
                </div>

                <div class="col-2">
                    <label>Link titel</label>
                    <div class="form-group">
                        <input type="text" class="form-control" maxlength="50" name="link_title" />
                    </div>
                </div>

                <div class="col-12">
                    <label>Beschrijving</label>
                    <div class="form-group">
                        <input type="text" class="form-control" maxlength="200" name="description" />
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <?php echo add_csrf_value(); ?>
                        <?php echo add_submit_button() ?>
                        <?php echo add_reset_button() ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<script>
    $("form#send").submit(function(e) {
        e.preventDefault();
        ajax_form_search($(this));
    });
</script>