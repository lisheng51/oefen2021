<div class="card">
    <div class="card-header">Selectie</div>
    <div class="card-body">
        <form method="POST" id="form_search">
            <div class="row">
                <div class="col-3">
                    <?php echo labelSelectSelectMulti('API naam', 'api_id', $this->api_model->selectMultiple(loadPostGet('api_id', 'array'))) ?>
                </div>
                <div class="col-3">
                    <?php echo labelSelectInput('IP', 'ip_address') ?>
                </div>
                <div class="col-3">
                    <?php echo labelSelectInput('POST', 'post_value') ?>
                </div>
                <div class="col-3">
                    <?php echo labelSelectInput('GET', 'get_value') ?>
                </div>
                <div class="col-3">
                    <?php echo labelSelectInput('Header', 'header_value') ?>
                </div>
                <!--                <div class="col-3">
                    <div class="form-group">
                        <label>Datum</label>
                        <input type="text" name="reportrange" class="form-control" />
                    </div> 
                </div>-->
                <div class="col-3">
                    <?php echo labelSelectInput('Path/url', 'path') ?>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>Datum&tijd begin</label>
                        <input type="text" name="reportrange_start" class="form-control" value="<?php echo loadPostGet('reportrange_start') ?>" />
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group">
                        <label>Datum&tijd eind</label>
                        <input type="text" name="reportrange_end" class="form-control" value="<?php echo loadPostGet('reportrange_end') ?>" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php echo add_csrf_value(); ?>
                    <?php echo search_button() ?>
                    <?php echo reset_button() ?>
                </div>
            </div>
        </form>
    </div>
    <div class="card-footer">
        <div class="row">
            <div class="col-2">
                <?php echo select_order_by($this->api_log_model->select_order_by); ?>
            </div>
            <div class="col-2">
                <?php echo select_page_limit() ?>
            </div>
        </div>
    </div>
</div>


<div class="row mt-3">
    <div class="col-12" id="ajax_search_content">
        <?php echo $result; ?>
    </div>
</div>


<script>
    $(function() {
        ajax_form_search($("form#form_search"));
    });

    $("form#form_search").submit(function(e) {
        ajax_form_search($(this));
        e.preventDefault();
    });
    //input_reportrange('input[name="reportrange"]');
    input_datewithtime('input[name="reportrange_start"]');
    input_datewithtime('input[name="reportrange_end"]');
</script>