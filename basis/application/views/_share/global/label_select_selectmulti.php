<div class="form-group">
    <label><?php echo $label ?></label>
    <div class="input-group">
        <div class="input-group-prepend">
            <?php echo $select ?>
        </div>
        <?php echo $selectMulti ?>
    </div>
</div>