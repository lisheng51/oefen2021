#
# TABLE STRUCTURE FOR: bc_api
#

CREATE TABLE `bc_api` (
  `api_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `secret` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max` smallint(5) NOT NULL DEFAULT 0,
  `token_min` smallint(4) unsigned NOT NULL DEFAULT 0,
  `createdby` int(10) NOT NULL DEFAULT 0,
  `modifiedby` int(10) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `is_del` tinyint(1) NOT NULL DEFAULT 0,
  `permission_group_ids` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`api_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (1, '82444824b84c183e9bfabb8daa95bc7d', 'Logipoort', 0, 50, 1, 1, '2019-06-27 11:37:18', '2019-07-08 10:45:06', 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (2, 'b89f0f6ec9189c8485f1d68a131c6702', 'Monitoring', 0, 100, 1, 0, '2019-06-27 14:58:28', NULL, 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (3, '266235cb90dfe95aa8aed921997b22cb', 'Uniekdating', 0, 100, 1, 0, '2019-06-27 14:58:28', NULL, 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (4, 'ca63be25600ead9dd7d7d588ab4ee425', 'Localhost', 0, 15, 1, 0, '2019-06-27 14:58:28', '2019-07-18 13:23:45', 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (5, '27978cc58e68bb516813913987b77d32', 'Logisal', 0, 100, 1, 0, '2019-08-01 11:42:28', NULL, 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (6, '11e80af6f700448cb53a529d8bc59cc6', 'Lis', 0, 100, 1, 0, '2019-08-01 11:42:37', NULL, 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (7, '4476054d6f3af2e3eb49732c964839a5', 'Windows', 0, 100, 1, 1, '2019-09-10 15:09:40', '2021-09-13 09:54:14', 0, '12');
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (8, '251d22c89e82ce235328381191056107', 'Logisal_Web', 0, 150, 1, 0, '2019-11-18 09:07:46', NULL, 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (9, '63678742d1b948968eea79b9c2098f24', 'Diflexx', 0, 100, 1, 0, '2019-09-10 15:09:40', NULL, 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (10, '9fb226f5e33d7ea28b77b1928159b3d7', 'Client_diflexx', 0, 150, 1, 0, '2019-11-18 09:07:46', NULL, 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (11, '9fcb005ce1db05b0b796ecab9078fcd3', 'Proveiling', 0, 30, 1, 0, '2020-09-01 13:50:38', NULL, 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (12, 'aa50e177b921e92bf7f0de280d6a7305', 'Proveiling azure', 0, 30, 1, 0, '2020-09-24 09:37:15', NULL, 0, NULL);
INSERT INTO `bc_api` (`api_id`, `secret`, `name`, `max`, `token_min`, `createdby`, `modifiedby`, `created_at`, `modified_at`, `is_del`, `permission_group_ids`) VALUES (13, '607c386ecfeb7c4931d1996b2fd0e6d2', 'Game', 0, 30, 1, 0, '2020-09-24 09:37:15', NULL, 0, NULL);


