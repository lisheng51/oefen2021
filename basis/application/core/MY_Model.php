<?php

class MY_Model extends CI_Model
{

    public $table = "";
    public $primary_key = "";

    public $sql_set_raw = [];

    public $sql_select = [];
    public $sql_select_raw = [];

    public $sql_where = [];
    public $sql_where_raw = [];

    public $sql_where_in = [];
    public $sql_where_in_raw = [];

    public $sql_like = [];
    public $sql_like_raw = [];

    public $sql_order_by = [];
    public $sql_order_by_raw = [];

    public $sql_join = [];
    public $join_left_tables = [];

    public $_field_createdby = 'createdby';
    public $_field_modifiedby = 'modifiedby';
    public $_field_deleted_at = 'deleted_at';
    public $_field_is_deleted = 'is_del';
    protected $using_softDel = false;


    public function __construct()
    {
        parent::__construct();
    }

    public function setSqlWhere(array $whereSet = [])
    {
        $newArray = [];
        foreach ($whereSet as $array) {
            if (is_array($array)) {
                foreach ($array as $k => $v) {
                    if (!is_null($v) && $v !== '') {
                        $newArray[$k] = $v === 'value_is_null' ? null : $v;
                    }
                }
            }
        }
        $this->sql_where = $newArray;
    }

    public function sqlReset()
    {
        $this->sql_set_raw = [];
        $this->sql_select = [];
        $this->sql_select_raw = [];
        $this->sql_where = [];
        $this->sql_where_raw = [];
        $this->sql_where_in = [];
        $this->sql_where_in_raw = [];
        $this->sql_join = [];
        $this->sql_like = [];
        $this->sql_like_raw = [];
        $this->sql_order_by = [];
        $this->sql_order_by_raw = [];
        $this->join_left_tables = [];
        $this->db->reset_query();
    }

    public function add(array $data = [])
    {
        if ($this->db->field_exists($this->_field_createdby, $this->table) === true) {
            $data[$this->_field_createdby] = $this->login_model->user_id();
        }
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function edit(int $id = 0, array $data = [])
    {
        if ($this->db->field_exists($this->_field_modifiedby, $this->table) === true) {
            $data[$this->_field_modifiedby] = $this->login_model->user_id();
        }

        $this->db->set($data);
        if (empty($this->sql_set_raw) === false) {
            foreach ($this->sql_set_raw as $field => $value) {
                $this->db->set($field, $value, false);
            }
        }
        $this->db->where($this->primary_key, $id);
        return $this->db->update($this->table);
    }

    public function del(int $id = 0)
    {
        if ($this->using_softDel === true) {
            $data[$this->_field_is_deleted] = 1;
            if ($this->db->field_exists($this->_field_deleted_at, $this->table) === true) {
                $data[$this->_field_deleted_at] = date('Y-m-d H:i:s');
            }
            return $this->edit($id, $data);
        }
        return $this->db->from($this->table)->where($this->primary_key, $id)->limit(1)->delete();
    }

    public function get_all(): array
    {
        $limit = $this->get_total();
        return $this->get_list($limit);
    }

    public function get_total(): int
    {
        $this->db->select($this->primary_key);
        $this->db->from($this->table);
        if (empty($this->sql_join) === false) {
            foreach ($this->sql_join as $result) {
                foreach ($result as $table => $field) {
                    $type = '';
                    if (in_array($table, $this->join_left_tables) === true) {
                        $type = 'left';
                    }
                    $this->db->join($table, $field, $type);
                }
            }
        }

        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_where_raw) === false) {
            foreach ($this->sql_where_raw as $field => $value) {
                $this->db->where($field, $value, false);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_like_raw) === false) {
            foreach ($this->sql_like_raw as $field => $value) {
                $this->db->like($field, $value, 'both', false);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_where_in_raw) === false) {
            foreach ($this->sql_where_in_raw as $field => $value) {
                $this->db->where_in($field, $value, false);
            }
        }

        return $this->db->count_all_results();
    }

    public function get_list(int $limit = 1, int $page = 0): array
    {
        $this->db->select($this->table . '.*');
        if (empty($this->sql_select) === false) {
            foreach ($this->sql_select as $result) {
                foreach ($result as $table => $field) {
                    $this->db->select($table . '.' . $field);
                }
            }
        }

        if (empty($this->sql_select_raw) === false) {
            foreach ($this->sql_select_raw as $select) {
                $this->db->select($select, false);
            }
        }

        $this->db->from($this->table);
        if (empty($this->sql_join) === false) {
            foreach ($this->sql_join as $result) {
                foreach ($result as $table => $field) {
                    $type = '';
                    if (in_array($table, $this->join_left_tables) === true) {
                        $type = 'left';
                    }
                    $this->db->join($table, $field, $type);
                }
            }
        }

        if (empty($this->sql_where) === false) {
            foreach ($this->sql_where as $field => $value) {
                $this->db->where($field, $value);
            }
        }

        if (empty($this->sql_where_raw) === false) {
            foreach ($this->sql_where_raw as $field => $value) {
                $this->db->where($field, $value, false);
            }
        }

        if (empty($this->sql_like) === false) {
            foreach ($this->sql_like as $field => $value) {
                $this->db->like($field, $value);
            }
        }

        if (empty($this->sql_like_raw) === false) {
            foreach ($this->sql_like_raw as $field => $value) {
                $this->db->like($field, $value, 'both', false);
            }
        }

        if (empty($this->sql_where_in) === false) {
            foreach ($this->sql_where_in as $field => $value) {
                $this->db->where_in($field, $value);
            }
        }

        if (empty($this->sql_where_in_raw) === false) {
            foreach ($this->sql_where_in_raw as $field => $value) {
                $this->db->where_in($field, $value, false);
            }
        }

        if (empty($this->sql_order_by_raw) === false) {
            foreach ($this->sql_order_by_raw as $field => $value) {
                $this->db->order_by($field, $value, false);
            }
        }

        if (empty($this->sql_order_by) === false) {
            foreach ($this->sql_order_by as $field => $value) {
                $this->db->order_by($field, $value);
            }
        }

        if (empty($this->sql_order_by) === true) {
            $this->db->order_by($this->primary_key, "desc");
        }

        if ($page <= 0) {
            $page = 0;
        }
        $this->db->limit($limit, $page);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_one(): array
    {
        $arr_result = $this->get_list();
        if (empty($arr_result) === false) {
            return current($arr_result);
        }
        return [];
    }

    public function get_one_by_id(int $id = 0): array
    {
        $data[$this->primary_key] = $id;
        $this->sql_where = $data;
        return $this->get_one();
    }

    public function get_one_by_field(string $field = '', $value = null): array
    {
        if (empty($field) === false && $value !== null) {
            $this->sql_where = [];
            $data[$field] = $value;
            $this->sql_where = $data;
        }
        return $this->get_one();
    }

    public function get_all_by_field(string $field = '', $value = null): array
    {
        if (empty($field) === false && $value !== null) {
            $this->sql_where = [];
            $data[$field] = $value;
            $this->sql_where = $data;
        }
        return $this->get_all();
    }

    public function fetch_field(int $id = 0, string $field = ''): string
    {
        $arr = $this->get_one_by_id($id);
        if (isset($arr[$field]) === true && empty($arr) === true) {
            return "";
        }
        return $arr[$field] ?? "";
    }
}
