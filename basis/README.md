## Omgeving
Apache 2.4;
PHP 8.0;
MariaDB 10.4
NodeJS 12; 
Composer 2;


## Voorbereiden
Maak een database aan, opent 'env.php', pas volgende database instelling aan:

$domainSetting['databaseHost'] ?? '...';     
$domainSetting['databaseName'] ?? '...';
$domainSetting['databaseUser'] ?? '...';
$domainSetting['databasePass'] ?? '...';


## Dependencies installeren
Opent PowerShell in root, voert volgende opdracht uit:
'npm i' 

Als klaar is, voert volgende opdracht uit:
'cd .\application\modules\webmaster\'
enter en dan voert volgende opdracht uit:
'composer i'

## Data installeren
Ga naar: /site/setup maak admin aan, daarna als webmaster inloggen
