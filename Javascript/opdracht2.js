$( document ).ready(function() {
    var request;

    $("#form").on("submit", function (event) {
        event.preventDefault();
        if (request) {
            request.abort();
        }

        var $form = $(this);
        var $inputs = $form.find("input");
        var serializedData = $form.serialize();
        $inputs.prop("disabled", true);

        request = $.ajax({
            url: "/oefen/fibonacci_results.php",
            type: "POST",
            dataType: 'json',
            data: serializedData
        });

        request.done(function (result) {
            $("#container-result").html(result.message);
        });

        request.always(function () {
            $inputs.prop("disabled", false);
        });
    });
});