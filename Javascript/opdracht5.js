$( document ).ready(function() {
    let request;

    $("#select-year").on("change", function() {
        console.log("Change")
        if (request) {
            request.abort();
        }

        let $select = $(this);
        let serializedData = $select.serialize();

        request = $.ajax({
            url: "/oefen/week.php",
            type: "POST",
            dataType: 'json',
            data: serializedData
        });

        request.done(function (result) {
            $("#select-week").html(result.html);
        });
    });
});