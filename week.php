<?php
/**
 * @return string Returns string of HTML options for the year select
 */
function GetYearHTML() : string {
    $html = '';
    $currentYear = date("Y");

    // Loop through years -2 to +2
    for ($counter = $currentYear - 2; $counter <= $currentYear + 2; $counter++) {
        if ($counter == $currentYear) {
            $html .= "<option selected>"; // Add opening tag
        } else {
            $html .= "<option>"; // Add opening tag
        }
        $html .= "{$counter}</option>"; // Add value + closing tag
    }
    return $html;
}

/**
 * @param int $year year in int that you want to return weeks for
 * @return string Returns string of HTML options for the week select
 */
function GetWeekSelectHTML(int $year) : string {
    $monthsDutch = ["Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "December"];
    $html = "<optgroup label='{$monthsDutch[0]}'>";
    $date = new DateTime();
    $currentWeek = $date->format("W");
    $date->setISODate($year, 1);
    while (True) {
        $week = $date->format("W"); // Current week
        $startMonth = $date->format("m"); // Month int of start of week
        $startDateString = $date->format("d-m-Y"); // Start week date as string
        $date->modify("+6 days"); // Move date to end of week
        $endMonth = $date->format("m"); // Month int of end of week
        $endDateString = $date->format("d-m-Y"); // End week date as string

        // Returns HTML when looped through the full year
        if ($week == 1 && $date->format("Y") == $year + 1) {
            return $html;
        }

        $selected = "";
        if ($currentWeek == $week) {
            $selected = "selected";
        }

        $html .= "<option {$selected}>Week: {$week} ({$startDateString} t/m {$endDateString})</option>";

        $date->modify("+1 day"); // Move to next week
        $nextWeeksMonth = $date->format("m"); // Next week's month
        if ($startMonth != $nextWeeksMonth && $nextWeeksMonth != 1 ) { // If next week is in a new month that isn't january
            $html .= "</optgroup>"; // Close last option group
            $html .= "<optgroup label='{$monthsDutch[$date->format('m')-1]}'>"; // Open new option group with new month's name

            if ($startMonth != $endMonth) { // If the week ends in a new month, add that week to the new month as well
                $html .= "<option>Week: {$week} ({$startDateString} t/m {$endDateString})</option>";
            }
        }
    }
}

if (isset($_POST["year"])) {
    $array["html"] = GetWeekSelectHTML($_POST["year"]);
    exit(json_encode($array));
}