<?php

class BingoCard
{
    private $numbersArray;

    /**
     * @param int $numbersCol The value at which each column's max value increases
     */
    function __construct(int $numbersCol) {
        $numbersCol = max(5, $numbersCol); // Minimum value of 25

        for ($counter = 0; $counter < 5; $counter++) {
            $array = range($numbersCol * $counter + 1, $numbersCol * ($counter + 1));
            shuffle($array);
            $this->numbersArray[] = array_splice($array, 0, 5);
        }
    }

    /**
     * @param int $x x position of number starting at 0
     * @param int $y y position of number starting at 0
     * @return int number at provided x and y coords
     */
    function GetNumberAt(int $x, int $y) : int {
        return $this->numbersArray[$x][$y];
    }

    /**
     * @return string String of numbers in V pattern across the bingo card
     */
    function GetVPattern() : string {
        return "{$this->GetNumberAt(0, 0)}, 
                {$this->GetNumberAt(1, 1)}, 
                {$this->GetNumberAt(2, 2)}, 
                {$this->GetNumberAt(3, 1)}, 
                {$this->GetNumberAt(4, 0)}";
    }

    /**
     * @return string String of numbers in W pattern across the bingo card
     */
    function GetWPattern() : string {
        return "{$this->GetNumberAt(0, 0)}, 
                {$this->GetNumberAt(1, 1)}, 
                {$this->GetNumberAt(2, 0)}, 
                {$this->GetNumberAt(3, 1)}, 
                {$this->GetNumberAt(4, 0)}";
    }

    /**
     * @return string String of all HTML for bingo card with info on V and W pattern
     */
    function GetCardHTML() : string {
        // Generate string of bingocard HTML
        $content = "";
        for ($y = 0; $y < 5; $y++) {
            $content .= "<tr>";
            for ($x = 0; $x < 5; $x++) {
                $content .= "<td> {$this->GetNumberAt($x, $y)} </td>";
            }
            $content .= "</tr>";
        }

        // Fill and return the HTML template
        $template = file_get_contents("Templates/bingocard.html");
        $result = str_replace(
            [
                '{content}',
                '{rowV}',
                '{rowW}'
            ],
            [
                $content,
                $this->GetVPattern(),
                $this->GetWPattern()
            ],
            $template
        );
        return $result;
    }
}